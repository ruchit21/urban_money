import { Pagination, Stack, TextField } from "@mui/material";
import React from "react";
import {
  Button,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import BorderColorOutlinedIcon from "@mui/icons-material/BorderColorOutlined";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

function createData(request, request_t, current_d, target_d, status) {
  return { request, request_t, current_d, target_d, status };
}

const rows = [
  createData("Designer", "Jr.Designer", 10000, 24000),
  createData("Designer", "Jr.Designer", 10000, 24000),
  createData("Designer", "Jr.Designer", 10000, 24000),
  createData("Designer", "Jr.Designer", 10000, 24000),
  createData("Designer", "Jr.Designer", 10000, 24000),
  createData("Designer", "Jr.Designer", 10000, 24000),
];

const AddCompany = (props) => {
  return (
    <div style={{ width: "100%", margin: "10px 0 10px 10px" }}>
      <div className="urban_money_page_content">
        <div className="urban_money_page_title">
          <h2 style={{ margin: "0" }}>
            <b> {props?.routeName} </b>
          </h2>
        </div>
        <div
          style={{
            margin: "20px",
            boxShadow: "0 .5rem 1rem rgba(0,0,0,.15)",
            borderRadius: "5px",
          }}
        >
          <div
            style={{
              background: "#fff",
              padding: "10px",
              borderRadius: "10px",
            }}
          >
            <Grid container rowSpacing={2} columnSpacing={2}>
              <Grid item lg={8}>
                <TextField
                  label="Name Of company "
                  placeholder="Groovy-Web"
                  variant="outlined"
                  fullWidth
                  color="success"
                />
              </Grid>
              <Grid item lg={4}>
                <TextField
                  label="No. Of employee "
                  placeholder="500"
                  variant="outlined"
                  fullWidth
                  color="success"
                />
              </Grid>
              <Grid item lg={3}>
                <TextField
                  label="Minimum salary"
                  placeholder="1,00,000"
                  variant="outlined"
                  fullWidth
                  color="success"
                />
              </Grid>
              <Grid item lg={3}>
                <TextField
                  label="Miximum salary "
                  placeholder="2,50,000"
                  variant="outlined"
                  fullWidth
                  color="success"
                />
              </Grid>
              <Grid item lg={3}>
                <TextField
                  label="Mean Salary "
                  placeholder="1,00,000"
                  variant="outlined"
                  fullWidth
                  color="success"
                />
              </Grid>
              <Grid item lg={3}>
                <TextField
                  label="Median Salary"
                  placeholder="1,00,000"
                  variant="outlined"
                  fullWidth
                  color="success"
                />
              </Grid>
              <Grid item lg={2.4}>
                <InputLabel
                  style={{
                    color: "#494949",
                    fontWeight: "bold",
                    fontSize: "17px",
                  }}
                >
                  {" "}
                  Address
                </InputLabel>
              </Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={6}>
                <TextField
                  label="Address line 1"
                  placeholder="Akshaya Nagar, 1st Block 1st Cross"
                  variant="outlined"
                  fullWidth
                  color="success"
                />
              </Grid>
              <Grid item lg={6}>
                <TextField
                  label="Address line 1"
                  placeholder="Rammurthy nagar"
                  variant="outlined"
                  fullWidth
                  color="success"
                />
              </Grid>
              <Grid item lg={3}>
                <TextField
                  label="State "
                  placeholder="Delhi"
                  variant="outlined"
                  fullWidth
                  color="success"
                />
              </Grid>
              <Grid item lg={3}>
                <TextField
                  label="City"
                  placeholder="New Delhi"
                  variant="outlined"
                  fullWidth
                  color="success"
                />
              </Grid>
              <Grid item lg={3}>
                <TextField
                  success
                  label="Pincode"
                  placeholder="111101"
                  variant="outlined"
                  fullWidth
                  color="success"
                  border="1px solid #009D93"
                />
              </Grid>
              <Grid item lg={3}></Grid>
              <Grid item lg={3}>
                <Button
                  fullWidth
                  variant="contained"
                  className="urban_money_filter_btn"
                  style={{ borderRadius: "4px" }}
                >
                  Save
                </Button>
              </Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}></Grid>
            </Grid>
          </div>
        </div>
        <div className="p-20">
          {" "}
          <Grid container columnSpacing={2}>
            <Grid item lg={9}>
              <InputLabel
                style={{
                  color: "#494949",
                  fontWeight: "bold",
                  fontSize: "17px",
                }}
              >
                {" "}
                Company Details
              </InputLabel>
            </Grid>
            <Grid item lg={2}>
              <Button
                fullWidth
                variant="contained"
                className="urban_money_btn m-0"
              >
                UPLOAD CSV
              </Button>
            </Grid>
            <Grid item lg={1}>
              <Button
                fullWidth
                variant="contained"
                className="urban_money_btn m-0"
                style={{ fontFamily: "PoppinsRegular", borderRadius: "4px" }}
              >
                <AddOutlinedIcon /> ADD
              </Button>
            </Grid>
          </Grid>
        </div>
        <div
          style={{
            margin: "20px",
            boxShadow: "0 .5rem 1rem rgba(0,0,0,.15)",
            borderRadius: "5px",
          }}
        >
          <TableContainer component={Paper}>
            <Table
              sx={{ minWidth: 650 }}
              size="small"
              aria-label="a dense table"
            >
              <TableHead>
                <TableRow>
                  <TableCell
                    style={{
                      color: "#009D93",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    DEPARTMENT
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      color: "#009D93",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    DESIGNATION
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      color: "#009D93",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    SALARY MINIMUN
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      color: "#009D93",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    SALARY MAXIMUN
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      color: "#009D93",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    SAMPLE DOCUMENTS
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      color: "#009D93",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    ACTION
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map((row) => (
                  <TableRow
                    key={row.name}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.request}
                    </TableCell>
                    <TableCell align="center">{row.request_t}</TableCell>
                    <TableCell align="center">{row.current_d}</TableCell>
                    <TableCell align="center">{row.target_d}</TableCell>
                    <TableCell align="center">
                      <Button
                        variant="outlined"
                        size="small"
                        className="urban_money_outlined_btn"
                      >
                        VIEW
                      </Button>
                    </TableCell>
                    <TableCell align="center">
                      <BorderColorOutlinedIcon
                        style={{ margin: "5px", color: "#009D93" }}
                      />
                      <DeleteOutlinedIcon
                        style={{ margin: "5px", color: "red" }}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <Stack spacing={2}>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                padding: "10px 20px 20px 20px",
              }}
            >
              <Pagination
                count={2}
                variant="outlined"
                shape="rounded"
                color="success"
              />

              <FormControl>
                <InputLabel color="success">10</InputLabel>
                <Select
                  // value={age}
                  label="10"
                  size="small"
                  color="success"
                  //   style={{ minWidth: '200px' }}
                >
                  <MenuItem value={10}>10</MenuItem>
                  <MenuItem value={20}>20</MenuItem>
                  <MenuItem value={30}>30</MenuItem>
                </Select>
              </FormControl>
            </div>
          </Stack>
        </div>
      </div>
    </div>
  );
};

export default AddCompany;
