import { TextField } from "@mui/material";
import React from "react";
import {
  Button,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import SearchBar from "material-ui-search-bar";
import Data from "../Component/Data";


const History = (props) => {
  return (
    <div style={{ width: "100%", margin: "10px 0 10px 10px" }}>
      <div className="urban_money_page_content">
        <div className="urban_money_page_title">
          <h2 style={{ margin: "0" }}>
            <b> {props?.routeName} </b>
          </h2>
        </div>
        <div style={{ padding: "20px" }}>
          <Grid container columnSpacing={2}>
            <Grid item lg={4}>
              <form noValidate>
                <TextField
                  id="date"
                  label="Birthday"
                  type="date"
                  fullWidth
                  placeholder="Start Date"
                  color="success"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </form>
            </Grid>
            <Grid item lg={4}>
              <form noValidate>
                <TextField
                  label="Birthday"
                  fullWidth
                  type="date"
                  placeholder="Start Date"
                  color="success"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </form>
            </Grid>
            <Grid item lg={4}>
              <SearchBar
                placeholder="Enter Keyword"
                fullWidth
                style={{
                  border: "1px solid #009D93",
                  height: "54px",
                  fontFamily: 'PoppinsRegular'
                }}
              />
            </Grid>
          </Grid>
        </div>
        <div style={{ padding: "20px" }}>
          <Grid container columnSpacing={2}>
            <Grid item lg={4}>
              <Grid container>
                <Grid item lg={8}>
                  <FormControl fullWidth>
                    <InputLabel style={{ fontFamily: 'PoppinsRegular' }} color="success">
                      Completed
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="Completed"
                      color="success"
                    >
                      <MenuItem value={10}>Ten</MenuItem>
                      <MenuItem value={20}>Twenty</MenuItem>
                      <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item lg={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    className="urban_money_filter_btn"
                    style={{ fontFamily: 'PoppinsRegular' }}
                  >
                    FILTER
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <Grid item lg={4}>
              <Grid container>
                <Grid item lg={8}>
                  <FormControl
                    fullWidth
                  >
                    <InputLabel style={{ fontFamily: 'PoppinsRegular' }} color="success">
                      Select
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="Select"
                      color="success"
                    >
                      <MenuItem value={10}>Ten</MenuItem>
                      <MenuItem value={20}>Twenty</MenuItem>
                      <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item lg={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    className="urban_money_filter_btn"
                    style={{ fontFamily: 'PoppinsRegular' }}
                  >
                    AGENT
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <Grid item lg={4}>

            </Grid>
          </Grid>
        </div>
        <Data />
      </div>
    </div>
  );
};

export default History;
