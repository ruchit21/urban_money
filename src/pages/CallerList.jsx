import React from 'react';
import SearchBar from "material-ui-search-bar";
import {
    Button,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
} from "@mui/material";
import Data from "../Component/Data";


const CallerList = (props) => {
    return (
        <div style={{ width: "100%", margin: "10px 0 10px 10px" }}>
            <div className="urban_money_page_content">
                <div className="urban_money_page_title">
                    <h2 style={{ margin: "0" }}>
                        <b className='poppins_semi'> {props?.routeName === "" ? "List" : "Caller List"} </b>
                    </h2>
                </div>
                <div
                    style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "space-between",
                        padding: "20px",
                    }}
                >
                    <InputLabel className='poppins_semi'>Allocated to me :</InputLabel>
                    <SearchBar
                        placeholder="Enter Keyword"
                        style={{
                            border: "1px solid #009D93",
                            minWidth: "512px",
                            height: "54px"
                        }}
                    />
                </div>

                <div style={{ padding: "20px" }}>
                    <Grid container columnSpacing={2}>
                        <Grid item lg={3}>
                            <Button
                                fullWidth
                                variant="contained"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Total: <InputLabel className='urban_money_contained_label poppins_semi'>47</InputLabel>
                            </Button>
                        </Grid>
                        <Grid item lg={3}>
                            <Button
                                fullWidth
                                variant="outlined"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Pending: <InputLabel className='urban_money_outlined_label poppins_semi'>10</InputLabel>
                            </Button>
                        </Grid>
                        <Grid item lg={3}>
                            <Button
                                fullWidth
                                variant="outlined"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Rejected: <InputLabel className='urban_money_outlined_label poppins_semi'>2</InputLabel>
                            </Button>
                        </Grid>
                        <Grid item lg={3}>
                            <Button
                                fullWidth
                                variant="outlined"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Forwarded to Approver: <InputLabel className='urban_money_outlined_label poppins_semi'>35</InputLabel>
                            </Button>
                        </Grid>
                    </Grid>
                </div>
                <Data />
                <div
                    style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "space-between",
                        padding: "20px",
                    }}
                >
                    <InputLabel className='poppins_semi'>Forwarded to Approver:</InputLabel>
                    <SearchBar
                        placeholder="Enter Keyword"
                        style={{
                            border: "1px solid #009D93",
                            minWidth: "512px",
                            height: "54px"
                        }}
                    />
                </div>
                <div style={{ padding: "20px" }}>
                    <Grid container columnSpacing={2}>
                        <Grid item lg={2.4}>
                            <Button
                                fullWidth
                                variant="contained"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Total: <InputLabel className='urban_money_contained_label poppins_semi'>35</InputLabel>
                            </Button>
                        </Grid>
                        <Grid item lg={2.4}>
                            <Button
                                fullWidth
                                variant="outlined"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Back to Caller: <InputLabel className='urban_money_outlined_label poppins_semi'>10</InputLabel>
                            </Button>
                        </Grid>
                        <Grid item lg={2.4}>
                            <Button
                                fullWidth
                                variant="outlined"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                With Approver: <InputLabel className='urban_money_outlined_label poppins_semi'>2</InputLabel>
                            </Button>
                        </Grid>
                        <Grid item lg={2.4}>
                            <Button
                                fullWidth
                                variant="outlined"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Forwarded to Approver: <InputLabel className='urban_money_outlined_label poppins_semi'>35</InputLabel>
                            </Button>
                        </Grid>
                        <Grid item lg={2.4}>
                            <Button
                                fullWidth
                                variant="outlined"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Forwarded to Approver: <InputLabel className='urban_money_outlined_label poppins_semi'>35</InputLabel>
                            </Button>
                        </Grid>
                    </Grid>
                </div>
                <Data />
            </div>
        </div>
    );
}

export default CallerList