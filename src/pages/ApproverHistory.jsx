import { TextField } from "@mui/material";
import React from "react";
import {
  Button,
  Grid,
  InputLabel,
} from "@mui/material";
import SearchBar from "material-ui-search-bar";
import Data from "../Component/Data";


const ApproverHistory = (props) => {
  return (

    <div style={{ width: "100%", margin: "10px 0 10px 10px" }}>
      <div className="urban_money_page_content">
        <div className="urban_money_page_title">
          <h2 style={{ margin: "0" }}>
            <b> {props?.routeName} </b>
          </h2>
        </div>
        <div style={{ padding: "20px" }}>
          <Grid container columnSpacing={2}>
            <Grid item lg={4}>
              <form noValidate>
                <TextField
                  id="date"
                  label="From date"
                  type="date"
                  fullWidth
                  placeholder="Start Date"
                  color="success"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </form>
            </Grid>
            <Grid item lg={4}>
              <form noValidate>
                <TextField
                  label="To date"
                  fullWidth
                  type="date"
                  placeholder="Start Date"
                  color="success"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </form>
            </Grid>
            <Grid item lg={4}>
              <SearchBar
                placeholder="Enter Keyword"
                fullWidth
                style={{
                  border: "1px solid #009D93",
                  height: "54px",
                  fontFamily: 'PoppinsRegular'
                }}
              />
            </Grid>
          </Grid>
        </div>
        <div style={{ padding: "10px 20px" }}>
          <div className="mb-15">
            <InputLabel className='poppins_semi'>Allocated to me:</InputLabel>
          </div>
          <Grid container columnSpacing={2}>
            <Grid container columnSpacing={2}>
              <Grid item lg={3}>
                <Button
                  fullWidth
                  variant="contained"
                  className="urban_money_filter_btn_color poppins_regular"
                >
                  Total: <InputLabel className='urban_money_contained_label poppins_semi'>80</InputLabel>
                </Button>
              </Grid>
              <Grid item lg={3}>
                <Button
                  fullWidth
                  variant="outlined"
                  className="urban_money_filter_btn_color poppins_regular"
                >
                  Checked: <InputLabel className='urban_money_outlined_label poppins_semi'>20</InputLabel>
                </Button>
              </Grid>
              <Grid item lg={3}>
                <Button
                  fullWidth
                  variant="outlined"
                  className="urban_money_filter_btn_color poppins_regular"
                >
                  Sent to Caller: <InputLabel className='urban_money_outlined_label poppins_semi'>21</InputLabel>
                </Button>
              </Grid>
              <Grid item lg={3}>
                <Button
                  fullWidth
                  variant="outlined"
                  className="urban_money_filter_btn_color poppins_regular"
                >
                  Completed: <InputLabel className='urban_money_outlined_label poppins_semi'>9</InputLabel>
                </Button>
              </Grid>
            </Grid>
            <Grid container columnSpacing={2} sx={{ my: 2 }}>
              <Grid item lg={3}>
                <Button
                  fullWidth
                  variant="outlined"
                  className="urban_money_filter_btn_color poppins_regular"
                >
                  Sent for Calling: <InputLabel className='urban_money_outlined_label poppins_semi'>4</InputLabel>
                </Button>
              </Grid>
              <Grid item lg={3}>
                <Button
                  fullWidth
                  variant="outlined"
                  className="urban_money_filter_btn_color poppins_regular"
                >
                  Approved: <InputLabel className='urban_money_outlined_label poppins_semi'>15</InputLabel>
                </Button>
              </Grid>
              <Grid item lg={3}>
                <Button
                  fullWidth
                  variant="outlined"
                  className="urban_money_filter_btn_color poppins_regular"
                >
                  Rejected: <InputLabel className='urban_money_outlined_label poppins_semi'>10</InputLabel>
                </Button>
              </Grid>
              <Grid item lg={3}>

              </Grid>
            </Grid>
          </Grid>
        </div>
        <Data />
      </div>
    </div>




  )
}

export default ApproverHistory