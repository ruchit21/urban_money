import { Box, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import React from 'react'
import Images from '../config/images';
import FormatListBulletedOutlinedIcon from '@material-ui/icons/FormatListBulletedOutlined';
import HistoryOutlinedIcon from '@material-ui/icons/HistoryOutlined';
import ApartmentOutlinedIcon from '@mui/icons-material/ApartmentOutlined';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import Drawer from '@mui/material/Drawer';
import { HiOfficeBuilding } from "react-icons/hi";

const { logo } = Images;
const drawerWidth = 300;

const Sidebar = () => {

    const navigate = useNavigate();
    const location = useLocation();

    const items = [
        {
            text: 'S2 List (Supervisor)',
            selected: true,
            route: "/",
            icon: <FormatListBulletedOutlinedIcon />,
            onclick: () => navigate("/")
        },
        {
            text: 'S2 List (Caller)',
            selected: true,
            route: "/caller",
            icon: <FormatListBulletedOutlinedIcon />,
            onclick: () => navigate("/caller-list")
        },
        {
            text: 'S2 List (Approver)',
            selected: true,
            route: "/approver",
            icon: <FormatListBulletedOutlinedIcon />,
            onclick: () => navigate("/approver-list")
        },
        {
            text: 'S2 History (Supervisor)',
            route: "/history",
            icon: <HistoryOutlinedIcon />,
            onclick: () => navigate("/history")
        },
        {
            text: 'S2 History  (Caller)',
            route: "/caller-history",
            icon: <HistoryOutlinedIcon />,
            onclick: () => navigate("/caller-history")
        },
        {
            text: 'S2 History (Approver)',
            route: "/approver-history",
            icon: <HistoryOutlinedIcon />,
            onclick: () => navigate("/approver-history")
        },
        {
            text: 'Company',
            route: "/company",
            icon: <ApartmentOutlinedIcon />,
            onclick: () => navigate("/company")
        },
        {
            text: 'College',
            route: "/college",
            icon: <HiOfficeBuilding />,
            onclick: () => navigate("/college")
        },
    ]

    return (
        <Box sx={{ display: "flex" }}>
            <Drawer
                variant="permanent"
                sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    "& .MuiDrawer-paper": {
                        width: drawerWidth,
                        boxSizing: "border-box",
                        background: 'transparent',
                        border: 'none',
                        overflowX: 'hidden'
                    },
                }}
            >
                <Link to={'/'}>
                    <img src={logo} alt="Urban_money_logo" height={120} width='100%' style={{ objectFit: 'contain', padding: '10px', marginTop: '21px' }} />
                </Link>

                <List className='urban_money_ul'>
                    {items.map((item, index) => {
                        const { text, icon, onclick, route } = item;
                        return (
                            <ListItem button key={text} onClick={onclick} className={location.pathname === route ? "active" : ""} style={{ marginBottom: "3px", marginLeft: '20px', borderRadius: '5px 0 0 5px', padding: '12px' }}>
                                <ListItemIcon style={{ minWidth: '40px' }}>{icon}</ListItemIcon>
                                <p style={{ fontFamily: 'PoppinsRegular', margin: '8px 0' }}>{text}</p>
                            </ListItem>
                        );
                    })}
                </List>
            </Drawer>
        </Box>
    )
}

export default Sidebar