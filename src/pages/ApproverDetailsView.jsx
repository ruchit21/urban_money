import React from "react";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Pagination,
  Select,
  Stack,
  TextField,
} from "@mui/material";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));
function createData(request, request_t, current_d, target_d, status, abc) {
  return { request, request_t, current_d, target_d, status, abc };
}

const rows = [
  createData("Frozen yoghurt", 159, 6.0, 24, 4.0, 12),
  createData("Ice cream sandwich", 237, 9.0, 37, 4.3, 12),
];

const ApproverDetailsView = (props) => {
  return (
    <div style={{ width: "100%" }}>
      <div className="urban_money_page_content_page">
        <div className="urban_money_page_title_page">
          <h2 style={{ margin: "0" }}>
            <b> {props?.routeName} </b>
          </h2>
        </div>
        <div className="p-20">
          <div
            style={{
              background: "#fff",
              padding: "10px",
              borderRadius: "10px",
            }}
          >
            <Grid container columnSpacing={2}>
              <Grid item lg={1}>
                <img
                  src="1.png"
                  alt="logo"
                  width="140"
                  height="128"
                  style={{
                    borderRadius: "4px",
                  }}
                />
              </Grid>

              <Grid item lg={11}>
                <Grid container rowSpacing={2} columnSpacing={2}>
                  <Grid item lg={2.4}>
                    <TextField
                      label="Name"
                      placeholder="Enter Name"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>
                  <Grid item lg={2.4}>
                    <TextField
                      label="Phone Number"
                      placeholder="Enter Phone Number"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>
                  <Grid item lg={2.4}>
                    <TextField
                      label="Alternate Number"
                      placeholder="Enter Alternate Number"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>
                  <Grid item lg={2.4}>
                    <TextField
                      label="Father's name"
                      placeholder="Enter Father's name"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>
                  <Grid item lg={2.4}>
                    <TextField
                      label="Mother's name"
                      placeholder="Enter Mother's name"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>

                  <Grid item lg={2.4}>
                    <TextField
                      label="Date of Birth"
                      placeholder="Enter Date of Birth"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>
                  <Grid item lg={2.4}>
                    <TextField
                      label="Age"
                      placeholder="Enter Age"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>
                  <Grid item lg={2.4}>
                    <TextField
                      label="Gender"
                      placeholder="Enter Gender"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>
                  <Grid item lg={2.4}>
                    <TextField
                      label="Marital Status"
                      placeholder="Enter Marital Status"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>
                  <Grid item lg={2.4}>
                    <Button
                      fullWidth
                      variant="contained"
                      className="urban_money_filter_btn"
                      style={{ borderRadius: "4px" }}
                    >
                      USER CONFIGURATION
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </div>
        </div>
        <div className="p-20">
          <div
            style={{
              background: "#fff",
              padding: "10px",
              borderRadius: "10px",
            }}
          >
            <Grid container columnSpacing={2}>
              <Grid item lg={12}>
                <Grid container columnSpacing={2}>
                  <Grid item lg={2.4}>
                    <TextField
                      label="Status"
                      placeholder="Registered"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>
                  <Grid item lg={2.4}>
                    <TextField
                      label="User type"
                      placeholder="Student"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>
                  <Grid item lg={2.4}>
                    <TextField
                      label="Profile status"
                      placeholder="Approved"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>
                  <Grid item lg={2.4}>
                    <TextField
                      label="Credit check"
                      placeholder="Approved"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>
                  <Grid item lg={2.4}>
                    <TextField
                      label="Salary Credit check "
                      placeholder="Approved"
                      variant="outlined"
                      fullWidth
                      color="success"
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </div>
        </div>
        <div className="p-20">
          <div
            style={{
              background: "#fff",
              padding: "10px",
              borderRadius: "10px",
            }}
          >
            <Grid container rowSpacing={2} columnSpacing={2}>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  CREDIT CHECK{" "}
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  SALARY CREDIT CHECK
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  fullWidth
                  variant="contained"
                  className="urban_theme_btn"
                  style={{ borderRadius: "4px" }}
                  size="large"
                >
                  DIVECE CHANGE REQUEST
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  BANK ACCOUNTS
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  LOAN ACCOUNTS
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  FILTER STUCK TICKETS
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  COIN MANAGEMENT
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  SUPPORT TICKET
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  fullWidth
                  variant="contained"
                  color="error"
                >
                  DISPERANCY
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  fullWidth
                  variant="contained"
                  color="error"
                >
                  CREDIT CHECK
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  fullWidth
                  variant="contained"
                  color="success"
                >
                  DEVICE CHANGE
                </Button>
              </Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}>
                <InputLabel
                  style={{
                    color: "#494949",
                    fontWeight: "bold",
                    fontSize: "17px",
                  }}
                >
                  {" "}
                  Contacts on Platform
                </InputLabel>
              </Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  LOGIN DEVICE
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  PRIMARY DEVICE
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  REFERRAL
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  CALL LOG
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  CONTACTS
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  COMMON EMAIL ON DEVICE
                </Button>
              </Grid>
              <Grid item lg={2.4}>
                <Button
                  size="large"
                  variant="outlined"
                  fullWidth
                  color="success"
                >
                  CALL LOG + CONTACT
                </Button>
              </Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}></Grid>
              <Grid item lg={2.4}></Grid>
            </Grid>
          </div>
        </div>

        <div className="p-20">
          <div
            style={{
              background: "#fff",
              padding: "10px",
              borderRadius: "10px",
            }}
          >
            <InputLabel
              style={{
                color: "#494949",
                fontWeight: "bold",
                fontSize: "17px",
                padding: "12px",
              }}
            >
              {" "}
              Device Change Request
            </InputLabel>

            <TableContainer component={Paper}>
              <Table
                sx={{ minWidth: 650 }}
                size="small"
                aria-label="a dense table"
              >
                <TableHead>
                  <TableRow>
                    <TableCell
                      style={{
                        color: "#009D93",
                        fontWeight: "bold",
                        fontSize: "14px",
                      }}
                    >
                      REQUEST ID
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{
                        color: "#009D93",
                        fontWeight: "bold",
                        fontSize: "14px",
                      }}
                    >
                      REQUEST TIMESTAMP
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{
                        color: "#009D93",
                        fontWeight: "bold",
                        fontSize: "14px",
                      }}
                    >
                      CURRENT DEVICE
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{
                        color: "#009D93",
                        fontWeight: "bold",
                        fontSize: "14px",
                      }}
                    >
                      TARGET DEVICE
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{
                        color: "#009D93",
                        fontWeight: "bold",
                        fontSize: "14px",
                      }}
                    >
                      STATUS
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{
                        color: "#009D93",
                        fontWeight: "bold",
                        fontSize: "14px",
                      }}
                    >
                      LAST ACTION STAMP WITH AGENT ID
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows.map((row) => (
                    <TableRow
                      key={row.name}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell component="th" scope="row">
                        {row.request}
                      </TableCell>
                      <TableCell align="center">{row.request_t}</TableCell>
                      <TableCell align="center">{row.current_d}</TableCell>
                      <TableCell align="center">{row.target_d}</TableCell>
                      <TableCell align="center">{row.status}</TableCell>
                      <TableCell align="center">{row.abc}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <Stack spacing={2}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                  padding: "10px 1px  40px",
                }}
              >
                <Pagination
                  count={2}
                  variant="outlined"
                  shape="rounded"
                  color="success"
                />

                <FormControl>
                  <InputLabel>10</InputLabel>
                  <Select
                    // value={age}
                    label="10"
                    size="small"
                    // onChange={handleChange}
                  >
                    <MenuItem value={10}>10</MenuItem>
                    <MenuItem value={20}>20</MenuItem>
                    <MenuItem value={30}>30</MenuItem>
                  </Select>
                </FormControl>
              </div>
            </Stack>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ApproverDetailsView;
