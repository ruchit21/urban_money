export const api_url = "http://localhost:4000";

export const setHeader = () => {
    const headers = {
        headers: {
            "Content-Type": "application/json",
            "lms-Authorization": "Bearer " + localStorage.getItem("token"),
        }
    };

    return headers;
}