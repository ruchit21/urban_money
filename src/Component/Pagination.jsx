import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            marginTop: theme.spacing(2),
        },
    },
}));

export default function PaginationRounded() {
    const classes = useStyles();
    const [page, setPage] = React.useState('');
    const handleChange = (event) => {
        setPage(event.target.value);
    };

    return (
        <div className={classes.root} style={{ display: "flex", alignItems: "center", justifyContent: "space-between", }}>
            <Pagination count={10} variant="outlined" shape="rounded" />
            <FormControl className={classes.margin} >
                <InputLabel style={{ fontFamily: 'PoppinsRegular' }} color="success">
                    limit
                </InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    // value={page}
                    // fullWidth
                    //label="Module"
                    color='success'
                    //onChange={handleChange}
                    style={{ padding: "0 20px" }}
                >
                    <MenuItem value="">
                        <em>None</em>
                    </MenuItem>
                    <MenuItem value={1}>1</MenuItem>
                    <MenuItem value={2}>2</MenuItem>
                    <MenuItem value={3}>3</MenuItem>
                    <MenuItem value={4}>4</MenuItem>
                    <MenuItem value={5}>5</MenuItem>
                    <MenuItem value={6}>6</MenuItem>
                    <MenuItem value={7}>7</MenuItem>
                    <MenuItem value={8}>8</MenuItem>
                    <MenuItem value={9}>9</MenuItem>
                    <MenuItem value={10}>10</MenuItem>
                </Select>
            </FormControl>


        </div>
    );
}
