import * as React from "react";
import PropTypes from "prop-types";
import { alpha } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import Toolbar from "@mui/material/Toolbar";
import Checkbox from "@mui/material/Checkbox";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";
import Chip from "@mui/material/Chip";
import KeyboardArrowDownOutlinedIcon from '@material-ui/icons/KeyboardArrowDownOutlined';

import { visuallyHidden } from "@mui/utils";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";

function createData(name, calories, fat, carbs, protein, abc) {
  return {
    name,
    calories,
    fat,
    carbs,
    protein,
    abc,
  };
}

const rows = [
  createData(
    "Karna Oberoi(123456)",
    <Stack direction="row" spacing={1}>
      <Chip label="KYC" sx={{ bgcolor: '#DE1717', color: 'white' }} />
      <Chip label="Link" sx={{ bgcolor: '#3EA467', color: 'white' }} />
      <Chip label="KYC" sx={{ bgcolor: '#DE1717', color: 'white' }} />
      <Chip label="Link" sx={{ bgcolor: '#3EA467', color: 'white' }} />
    </Stack>,
    3.7,
    67,
    4.3,
    45
  ),
  createData(
    "Donut",
    <Stack direction="row" spacing={1}>
      <Chip label="KYC" sx={{ bgcolor: '#DE1717', color: 'white' }} />
      <Chip label="Link" sx={{ bgcolor: '#3EA467', color: 'white' }} />
      <Chip label="KYC" sx={{ bgcolor: '#DE1717', color: 'white' }} />
      <Chip label="Link" sx={{ bgcolor: '#3EA467', color: 'white' }} />
    </Stack>,
    25.0,
    51,
    4.9,
    50
  ),
  createData(
    "Eclair",
    <Stack direction="row" spacing={1}>
      <Chip label="KYC" sx={{ bgcolor: '#DE1717', color: 'white' }} />
      <Chip label="Link" sx={{ bgcolor: '#3EA467', color: 'white' }} />
      <Chip label="KYC" sx={{ bgcolor: '#DE1717', color: 'white' }} />
      <Chip label="Link" sx={{ bgcolor: '#3EA467', color: 'white' }} />
    </Stack>,
    16.0,
    24,
    6.0,
    55
  ),
  createData(
    "Frozen yoghurt",
    <Stack direction="row" spacing={1}>
      <Chip label="KYC" sx={{ bgcolor: '#DE1717', color: 'white' }} />
      <Chip label="Link" sx={{ bgcolor: '#3EA467', color: 'white' }} />
      <Chip label="KYC" sx={{ bgcolor: '#DE1717', color: 'white' }} />
      <Chip label="Link" sx={{ bgcolor: '#3EA467', color: 'white' }} />
    </Stack>,
    6.0,
    24,
    4.0,
    60
  ),
  createData(
    "Gingerbread",
    <Stack direction="row" spacing={1}>
      <Chip label="KYC" sx={{ bgcolor: '#DE1717', color: 'white' }} />
      <Chip label="Link" sx={{ bgcolor: '#3EA467', color: 'white' }} />
      <Chip label="KYC" sx={{ bgcolor: '#DE1717', color: 'white' }} />
      <Chip label="Link" sx={{ bgcolor: '#3EA467', color: 'white' }} />
    </Stack>,
    16.0,
    49,
    3.9,
    65
  )
];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: "name",
    numeric: false,
    disablePadding: true,
    label: "USER DETAILS",
  },
  {
    id: "calories",
    numeric: true,
    disablePadding: false,
    label: "MODULE STATUS",
  },
  {
    id: "fat",
    numeric: true,
    disablePadding: false,
    label: "USER REGISTRATION TIMESTAMP",
  },
  {
    id: "carbs",
    numeric: true,
    disablePadding: false,
    label: "USER LAST ACTIONED AT",
  },
  {
    id: "protein",
    numeric: true,
    disablePadding: false,
    label: "AGENT LAST ACTIONED",
  },
  {
    id: "abc",
    numeric: true,
    disablePadding: false,
    label: "LAST CALL STATUS",
  },
];

function EnhancedTableHead(props) {
  const {
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort,
  } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow >
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{
              "aria-label": "select all desserts",
            }}
            style={{ color: '#009D93' }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "left" : "left"}
            padding={headCell.disablePadding ? "none" : "normal"}
            sortDirection={orderBy === headCell.id ? order : false}
            style={{ color: '#009D93', fontFamily: 'PoppinsSemiBold' }}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
              style={{ color: '#009D93' }}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const EnhancedTableToolbar = (props) => {
  const { numSelected } = props;

  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
        ...(numSelected > 0 && {
          bgcolor: (theme) =>
            alpha(
              theme.palette.primary.main,
              theme.palette.action.activatedOpacity
            ),
        }),
      }}
    >


      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton>{/* <DeleteIcon /> */}</IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton>{/* <FilterListIcon /> */}</IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

export default function EnhancedTable() {
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("calories");
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };

  const isSelected = (name) => selected.indexOf(name) !== -1;

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  return (
    <Box sx={{ width: "100%" }}>
      <div style={{ margin: '20px', boxShadow: '0 .5rem 1rem rgba(0,0,0,.15)', borderRadius: '5px' }}>
        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            size={dense ? "small" : "medium"}
          >
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.name);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      onClick={(event) => handleClick(event, row.name)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.name}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{
                            "aria-labelledby": labelId,
                          }}
                          style={{ color: '#009D93' }}
                        />
                      </TableCell>
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="none"
                      >
                        {row.name}
                      </TableCell>
                      <TableCell align="left">{row.calories}</TableCell>
                      <TableCell align="left">{row.fat}</TableCell>
                      <TableCell align="left">{row.carbs}</TableCell>
                      <TableCell align="left">{row.protein}</TableCell>
                      <TableCell align="left">{row.abc}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: (dense ? 33 : 53) * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
      <Stack spacing={2}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            padding: '10px 20px 20px 20px'
          }}
        >
          <Pagination
            count={2}
            variant="outlined"
            shape="rounded"
            color="success"
          />

          <FormControl>
            <InputLabel>10</InputLabel>
            <Select
              // value={age}
              label="10"
              fullWidth
              style={{ minWidth: '200px' }}
            >
              <MenuItem value={10}>10</MenuItem>
              <MenuItem value={20}>20</MenuItem>
              <MenuItem value={30}>30</MenuItem>
            </Select>
          </FormControl>
        </div>
      </Stack>
    </Box>
  );
}
