import SearchBar from "material-ui-search-bar";
import React from "react";

import {
  Button,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import Data from "../Component/Data";

const List = (props) => {
  return (
    <div style={{ width: "100%", margin: "10px 0 10px 10px" }}>
      <div className="urban_money_page_content">
        <div className="urban_money_page_title">
          <h2 style={{ margin: "0" }}>
            <b style={{ fontFamily: 'PoppinsSemiBold' }}> {props?.routeName === "" ? "List" : ""} </b>
          </h2>
        </div>

        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "end",
            padding: "20px",
          }}
        >
          <SearchBar
            placeholder="Enter Keyword"
            style={{
              border: "1px solid #009D93",
              minWidth: "512px",
              height: "54px"
            }}
          />
        </div>

        <div style={{ padding: "20px" }}>
          <Grid container columnSpacing={2}>
            <Grid item lg={4}>
              <Grid container>
                <Grid item lg={8}>
                  <FormControl
                    fullWidth
                  >
                    <InputLabel style={{ fontFamily: 'PoppinsRegular' }} color="success">
                      Completed
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="Completed"
                      color="success"
                    >
                      <MenuItem value={10}>Ten</MenuItem>
                      <MenuItem value={20}>Twenty</MenuItem>
                      <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item lg={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    className="urban_money_filter_btn"
                    style={{ fontFamily: 'PoppinsRegular' }}
                  >
                    FILTER
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <Grid item lg={4}>
              <Grid container>
                <Grid item lg={8}>
                  <FormControl
                    fullWidth

                  >
                    <InputLabel style={{ fontFamily: 'PoppinsRegular' }} color="success">
                      Select
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="Select"
                      color="success"
                    >
                      <MenuItem value={10}>Ten</MenuItem>
                      <MenuItem value={20}>Twenty</MenuItem>
                      <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item lg={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    className="urban_money_filter_btn"
                    style={{ fontFamily: 'PoppinsRegular' }}
                  >
                    ASSIGNED TO
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <Grid item lg={4}>
              <Grid container>
                <Grid item lg={8}>
                  <FormControl
                    fullWidth

                  >
                    <InputLabel style={{ fontFamily: 'PoppinsRegular' }} color="success">
                      Select
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="Select"
                      color="success"
                    >
                      <MenuItem value={10}>Ten</MenuItem>
                      <MenuItem value={20}>Twenty</MenuItem>
                      <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item lg={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    className="urban_money_filter_btn"
                    style={{ fontFamily: 'PoppinsRegular' }}
                  >
                    ASSIGN TO
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </div>
        <Data />
      </div>
    </div>
  );
};

export default List;
