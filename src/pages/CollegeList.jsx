import {
    Button,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
} from '@mui/material';
import React from 'react';
import SearchBar from "material-ui-search-bar";
import AddIcon from '@material-ui/icons/Add';
import { Box } from '@material-ui/core';
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import PropTypes from "prop-types";
import RemoveRedEyeOutlinedIcon from '@material-ui/icons/RemoveRedEyeOutlined';
import BorderColorOutlinedIcon from '@material-ui/icons/BorderColorOutlined';
import ShareOutlinedIcon from '@material-ui/icons/ShareOutlined';
import { RiDeleteBin5Line } from "react-icons/ri";
import DeleteIcon from '@material-ui/icons/Delete';
import { Link } from "react-router-dom";
import PaginationRounded from '../Component/Pagination';

function EnhancedTableHead(props) {
    const {
        onRequestSort,
    } = props;


    const headCells = [
        {
            id: "college",
            numeric: false,
            disablePadding: true,
            label: "COLLEGE",
        },
        {
            id: "no of students",
            numeric: true,
            disablePadding: false,
            label: "NO OF STUDENTS",
        },
        {
            id: "city",
            numeric: true,
            disablePadding: false,
            label: "CITY",
        },
        {
            id: "state",
            numeric: true,
            disablePadding: false,
            label: "STATE",
        },
        {
            id: "pincode",
            numeric: true,
            disablePadding: false,
            label: "PINCODE",
        },
        {
            id: "actions",
            numeric: true,
            disablePadding: false,
            label: "ACTIONS",
        },
    ];

    return (
        <TableHead>
            <TableRow >
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align={headCell.numeric ? "left" : "left"}
                        //padding={headCell.disablePadding ? "none" : "normal"}
                        //sortDirection={orderBy === headCell.id ? order : false}
                        style={{ color: '#009D93', fontFamily: 'PoppinsSemiBold' }}
                    >
                        {headCell.label}
                    </TableCell>
                ))}

            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    rowCount: PropTypes.number.isRequired,
};


const CollegeList = (props) => {

    function createData(name, no_of_students, city, state, pincode, actions) {
        return {
            name,
            no_of_students,
            city,
            state,
            pincode,
            actions,
        };
    }

    const College_data = [
        createData(
            "University of Calcutta",
            500,
            "Kolkata",
            "West Bengal",
            700073,
        ),
        createData(
            "Amity University, Kolkata",
            450,
            "Kolkata",
            "West Bengal",
            700073
        ),
        createData(
            "President University",
            500,
            "Kolkata",
            "West Bengal",
            700073,
        ),
        createData(
            "Indian Statiscial Insitute",
            450,
            "Kolkata",
            "West Bengal",
            700073,
        ),
        createData(
            "University of Calcutta",
            400,
            "Kolkata",
            "West Bengal",
            700073,
        )
    ];

    return (
        <div style={{ width: "100%", margin: "10px 0 10px 10px" }}>
            <div className="urban_money_page_content">
                <div className="urban_money_page_title">
                    <h2 style={{ margin: "0" }}>
                        <b> {props?.routeName} </b>
                    </h2>
                </div>
                <div style={{ padding: "20px" }}>
                    <Grid container columnSpacing={2}>
                        <Grid item lg={3.33}>
                            <Grid container>
                                <Grid item lg={6}>
                                    <FormControl fullWidth>
                                        <InputLabel style={{ fontFamily: 'PoppinsRegular' }} color="success">
                                            No of student
                                        </InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            label="No of student"
                                            color="success"
                                        >
                                            <MenuItem value={10}>0-100</MenuItem>
                                            <MenuItem value={20}>100-200</MenuItem>
                                            <MenuItem value={30}>200-300</MenuItem>
                                            <MenuItem value={40}>300-400</MenuItem>
                                            <MenuItem value={50}>400-500</MenuItem>
                                            <MenuItem value={60}>500-600</MenuItem>
                                            <MenuItem value={70}>600-700</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item lg={6}>
                                    <Button
                                        fullWidth
                                        variant="contained"
                                        className="urban_money_filter_btn"
                                        style={{ fontFamily: 'PoppinsRegular' }}
                                    >
                                        No of student
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item lg={3.33}>
                            <Grid container>
                                <Grid item lg={6}>
                                    <FormControl fullWidth>
                                        <InputLabel style={{ fontFamily: 'PoppinsRegular' }} color="success">
                                            All
                                        </InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            label="All "
                                            color="success"
                                        >
                                            <MenuItem value={10}>All</MenuItem>
                                            <MenuItem value={20}>Monthly</MenuItem>
                                            <MenuItem value={30}>Weekly</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item lg={6}>
                                    <Button
                                        fullWidth
                                        variant="contained"
                                        className="urban_money_filter_btn"
                                        style={{ fontFamily: 'PoppinsRegular' }}
                                    >
                                        Attendence type
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item lg={3.33}>
                            <SearchBar
                                placeholder="Search.."
                                fullWidth
                                style={{
                                    border: "1px solid #009D93",
                                    height: "54px",
                                    fontFamily: 'PoppinsRegular'
                                }}
                            />
                        </Grid>
                        <Grid item lg={2}>
                            <Link style={{ textDecoration: "none", color: "white" }} to={"add-college"}>
                                <Button
                                    fullWidth
                                    variant="contained"
                                    className="urban_money_filter_btn btn-border-radius"
                                    style={{ fontFamily: 'PoppinsRegular' }}
                                >
                                    <AddIcon style={{ marginRight: "5px" }} />
                                    Add

                                </Button>
                            </Link>
                        </Grid>
                    </Grid>
                    <Box sx={{ width: "100%" }}>
                        <div style={{ margin: '20px 0', boxShadow: '0 .5rem 1rem rgba(0,0,0,.15)', borderRadius: '5px' }}>
                            <TableContainer>
                                <Table
                                    sx={{ minWidth: 750 }}
                                    aria-labelledby="tableTitle"
                                >
                                    <EnhancedTableHead
                                        rowCount={College_data.length}
                                    />
                                    <TableBody>
                                        {
                                            College_data.map((row, index) => {
                                                return (
                                                    <TableRow
                                                        hover
                                                        tabIndex={-1}
                                                        key={row.name}
                                                    >

                                                        <TableCell>
                                                            {row.name}
                                                        </TableCell>
                                                        <TableCell align="left">{row.no_of_students}</TableCell>
                                                        <TableCell align="left">{row.city}</TableCell>
                                                        <TableCell align="left">{row.state}</TableCell>

                                                        <TableCell align="left">{row.pincode}</TableCell>
                                                        <TableCell >
                                                            <RemoveRedEyeOutlinedIcon style={{ color: '#009D93', margin: "5px" }} />
                                                            <BorderColorOutlinedIcon style={{ color: '#009D93', margin: "5px" }} />
                                                            <ShareOutlinedIcon style={{ color: '#009D93', margin: "5px" }} />
                                                            <DeleteIcon style={{ color: '#DE1717', margin: "5px" }} />
                                                        </TableCell>
                                                    </TableRow>
                                                );
                                            })}

                                    </TableBody>
                                </Table>
                            </TableContainer>
                            <div>
                                <PaginationRounded />
                            </div>
                        </div>
                    </Box>
                </div>
            </div>
        </div>

    )
}

export default CollegeList