import { Button } from '@material-ui/core'
import React, { useState } from 'react'
import Modal from '../Component/Modals'
import { BiPhoneCall } from "react-icons/bi";
import { Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Checkbox from '@material-ui/core/Checkbox';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import Images from '../config/images';
import PaginationRounded from '../Component/Pagination';

const { user } = Images;

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            <Box p={3}>{children}</Box>
        </Typography>
    );
}

function notificationData(module, comment, time_stamp, seen, agent) {
    return { module, comment, time_stamp, seen, agent };
}

function smstemplateData(content, action) {
    return { content, action };
}

function smsData(content, time_stamp, delivery_status, agent) {
    return { content, time_stamp, delivery_status, agent }
}

const notification_data = [
    notificationData('KYC details', 'Please...', '19-08-2021 | 18:00', '19-08-2021 | 18:00', 'Kabir (Caller - AC5749'),
    notificationData('Address details', 'Please...', '-', '-', 'Jitendra (Approver - AA90744'),
    notificationData('Selfie details', 'Please...', '10-08-2021 | 09:00', '-', 'Jitendra (Approver - AA90744'),
    notificationData('...', '...', '...', '....', '...'),
    notificationData('...', '...', '...', '....', '...'),
    notificationData('...', '...', '...', '....', '...'),
];

const sms_data = [
    smstemplateData('please upload a valid KYC document', 'true'),
    smstemplateData('please provide a proper college document', 'true'),
    smstemplateData('please capture a proper selfie video', 'true'),
    smstemplateData('...', 'false'),
    smstemplateData('...', 'false'),
    smstemplateData('...', 'false')
]

const sms_history_data = [
    smsData('Please...', '19-08-2021 | 18:00', 'Delivered', 'Kabir (Caller - AC5749)'),
    smsData('Please...', '20-07-2021 | 11:42', 'Un-Delivered', 'Jitendra (Approver - AA90744)'),
    smsData('...', '...', '...', '...'),
    smsData('...', '...', '...', '...'),
    smsData('...', '...', '...', '...'),
    smsData('...', '...', '...', '...'),
]

const DetailView = (props) => {

    const [open, setOpen] = useState(false);
    const [type, setType] = useState('');
    const [checked, setChecked] = React.useState(true);

    const [page, setPage] = React.useState('');

    const handleChange = (event, newValue) => {
        setChecked(event.target.checked);
        setValue(newValue);
        setPage(event.target.value);
    };



    const [value, setValue] = React.useState(0);

    // const handleChange = (event, newValue) => {
    //     setValue(newValue);
    // };


    TabPanel.propTypes = {
        children: PropTypes.node,
        index: PropTypes.any.isRequired,
        value: PropTypes.any.isRequired
    };

    function a11yProps(index) {
        return {
            id: `scrollable-auto-tab-${index}`,
            "aria-controls": `scrollable-auto-tabpanel-${index}`
        };
    }


    const style_green = {
        backgroundColor: "#3EA467",
        margin: "5px",
        padding: "10px",
        border: "none",
        fontWeight: "600",
    }

    const style_red = {
        backgroundColor: "#DE1717",
        margin: "5px",
        padding: "10px",
        border: "none",
        color: "#fff",
        fontWeight: "600",
    }

    const tab_style = {
        color: "#009D93",
        fontSize: "17px",
        fontFamily: 'PoppinsSemiBold'
    }

    const label_style = {
        fontFamily: 'PoppinsMedium',
        color: "#fff",
    }
    return (
        <div style={{ width: '100%', margin: '10px 0 10px 10px' }}>
            <div className='urban_money_page_content' style={{ backgroundColor: '#DFFEFC' }}>
                <div className='urban_money_page_title remove_border'>
                    <h2 style={{ margin: '0' }}><b className='poppins_semi'> {props?.routeName} </b></h2>
                </div>
                {/* Approval Section --- start*/}
                <div style={{ background: "white", margin: "15px", padding: "5px", borderRadius: "4px", }} >
                    <div style={{ display: "flex", alignItems: "center", justifyContent: "space-between", margin: "5px", fontWeight: "bold" }}>
                        <Grid container columnSpacing={2}>
                            <Grid item lg={1}>
                                <img src={user} alt="User Image" style={{ height: '100%', width: '100%' }} />
                            </Grid>
                            <Grid item lg={11}>
                                <Grid container rowSpacing={3} columnSpacing={2}>
                                    <Grid item lg={6}>
                                        <InputLabel className='poppins_semi' style={{ color: "#494949", fontSize: "22px", padding: "14px 0" }}> Approved sections:</InputLabel>
                                    </Grid>
                                    <Grid item lg={6}>
                                        <Grid container columnSpacing={2} >
                                            <Grid item lg={2}></Grid>
                                            <Grid item lg={5}>
                                                <Button variant="outlined" color="primary" fullWidth
                                                    style={{
                                                        color: "#009D93",
                                                        borderColor: "#009D93",
                                                        margin: "5px",
                                                        padding: "10px 0px",
                                                        fontFamily: 'PoppinsMedium'
                                                    }}
                                                >
                                                    Call History
                                                </Button>
                                            </Grid>
                                            <Grid item lg={5}>
                                                <Button
                                                    variant="contained" fullWidth
                                                    style={{
                                                        background: "transparent linear-gradient(180deg, #3EA467 0%, #0B90A5 100%) 0% 0% no-repeat padding-box",
                                                        color: "white",
                                                        margin: "5px",
                                                        padding: "10px 0px",
                                                        fontFamily: 'PoppinsMedium'
                                                    }} >
                                                    <BiPhoneCall size={20} style={{ margin: "5px" }} />
                                                    Click to call
                                                </Button>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item lg={1.7}>
                                        <Button fullWidth variant="contained" style={style_green}>
                                            <InputLabel style={label_style}>KYC</InputLabel>
                                        </Button>
                                    </Grid>
                                    <Grid item lg={1.7}>
                                        <Button fullWidth variant="contained" style={style_green} >
                                            <InputLabel style={label_style}>PAN</InputLabel>
                                        </Button>
                                    </Grid>
                                    <Grid item lg={1.7}>
                                        <Button fullWidth variant="contained" style={style_red}>
                                            <InputLabel style={label_style}>Personal</InputLabel>
                                        </Button>
                                    </Grid>
                                    <Grid item lg={1.7}>
                                        <Button fullWidth variant="contained" style={style_red}>
                                            <InputLabel style={label_style}>Student</InputLabel>
                                        </Button>
                                    </Grid>
                                    <Grid item lg={1.7}>
                                        <Button fullWidth variant="contained" style={style_green}>
                                            <InputLabel style={label_style}>Selfie</InputLabel>
                                        </Button>
                                    </Grid>
                                    <Grid item lg={1.7}>
                                        <Button fullWidth variant="contained" style={style_red}>
                                            <InputLabel style={label_style}>Video</InputLabel>
                                        </Button>
                                    </Grid>
                                    <Grid item lg={1.7}>
                                        <Button fullWidth variant="contained" style={style_green}>
                                            <InputLabel style={label_style}>Bits & Pieces</InputLabel>
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </div>
                </div>

                <div style={{ background: "white", margin: "15px", padding: "10px", borderRadius: "10px", }}>
                    <InputLabel className='poppins_semi' style={{ color: '#494949', fontSize: '22px' }}> Send SMS or Notification </InputLabel>

                    <div className='tab-title' style={{ border: "1px solid #009D93", borderRadius: "5px", margin: "10px 0" }}>
                        <Tabs
                            value={value}
                            onChange={handleChange}
                            variant="fullWidth"
                            className='details_tab'
                        >
                            <Tab label="Notifications" {...a11yProps(0)}
                                style={tab_style} />
                            <Tab label="SMS" {...a11yProps(1)}
                                style={tab_style} />
                        </Tabs>
                    </div>

                    <div className='tab-content detail' >
                        <TabPanel className="notification-tab-section" value={value} index={0} >
                            <Grid container columnSpacing={2}>
                                <Grid item lg={4}>
                                    <div style={{ border: '1px solid #009D93', borderRadius: '5px', padding: '10px' }}>
                                        <InputLabel className='poppins_semi' style={{ color: "#494949", fontSize: "15px", marginBottom: '32px' }}>In app communication</InputLabel>
                                        <div className='mt-10'>
                                            <FormControl fullWidth size='small'>
                                                <InputLabel className='poppins_regular' color="success">
                                                    Module
                                                </InputLabel>
                                                <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={page}
                                                    fullWidth
                                                    label="Module"
                                                    color='success'
                                                >
                                                    <MenuItem value="">
                                                        <em>None</em>
                                                    </MenuItem>
                                                    <MenuItem value={1}>KYC</MenuItem>
                                                    <MenuItem value={2}>2</MenuItem>
                                                </Select>
                                            </FormControl>
                                        </div>
                                        <div className='mt-10'>
                                            <TextField
                                                id="outlined-multiline-static"
                                                label="Comment"
                                                multiline
                                                fullWidth
                                                color='success'
                                                style={{ fontFamily: 'PoppinsMedium' }}
                                                rows={6}
                                            />
                                        </div>
                                        <Button className='urban_money_btn' variant="contained" fullWidth>SEND</Button>
                                    </div>
                                </Grid>
                                <Grid item lg={8}>
                                    <div style={{ border: '1px solid #009D93', borderRadius: '5px', padding: '10px' }}>
                                        <Grid container columnSpacing={2}>
                                            <Grid item lg={10}>
                                                <InputLabel className='poppins_semi' style={{ color: "#494949", fontSize: '15px' }}>In app sent</InputLabel>
                                            </Grid>

                                            <Grid item lg={2}>
                                                <Button fullWidth variant='contained' className='urban_money_btn m-0'>CLEAR IN APP</Button>
                                            </Grid>
                                        </Grid>

                                        <TableContainer style={{ marginTop: '5px' }}>
                                            <Table size='small' aria-label="simple table">
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell className='poppins_semi' style={{ color: '#009D93', paddingLeft: '0' }} align="left">MODULE</TableCell>
                                                        <TableCell className='poppins_semi' style={{ color: '#009D93' }} align="left">COMMENT</TableCell>
                                                        <TableCell className='poppins_semi' style={{ color: '#009D93' }} align="left">TIME STAMP</TableCell>
                                                        <TableCell className='poppins_semi' style={{ color: '#009D93' }} align="left">SEEN</TableCell>
                                                        <TableCell className='poppins_semi' style={{ color: '#009D93' }} align="left">AGENT</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {notification_data.map((row) => (
                                                        <TableRow
                                                            key={row.module}
                                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                        >
                                                            <TableCell component="th" scope="row" style={{ paddingLeft: '0' }}>
                                                                {row.module}
                                                            </TableCell>
                                                            <TableCell align="left">{row.comment}</TableCell>
                                                            <TableCell align="left">{row.time_stamp}</TableCell>
                                                            <TableCell align="left">{row.seen}</TableCell>
                                                            <TableCell align="left">{row.agent}</TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                                <PaginationRounded />
                                            </Table>
                                        </TableContainer>
                                    </div>
                                </Grid>
                            </Grid>
                        </TabPanel>

                        <TabPanel className="sms-tab-section" value={value} index={1}>
                            <Grid container columnSpacing={2}>
                                <Grid item lg={4}>
                                    <div style={{ border: '1px solid #009D93', borderRadius: '5px', padding: '10px' }}>
                                        <InputLabel className='poppins_semi' style={{ color: "#494949", fontSize: "15px", marginBottom: '10px' }}>SMS Templates</InputLabel>
                                        <TableContainer>
                                            <Table size='small' aria-label="simple table">
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell className='poppins_semi' style={{ color: '#009D93', paddingLeft: '0' }} align="left">SMS CONTENT</TableCell>
                                                        <TableCell className='poppins_semi' style={{ color: '#009D93' }} align="left">ACTION</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {sms_data.map((row) => (
                                                        <TableRow
                                                            key={row.content}
                                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                        >
                                                            <TableCell component="th" scope="row" style={{ paddingLeft: '0' }}>
                                                                {row.content}
                                                            </TableCell>
                                                            <TableCell align="left">
                                                                {
                                                                    row.action === 'true' ? <Checkbox
                                                                        style={{ color: '#009D93', padding: '0' }}
                                                                    /> : '...'
                                                                }

                                                            </TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                        <Button variant='contained' className='urban_money_btn mt-7' fullWidth>SEND</Button>
                                    </div>
                                </Grid>

                                <Grid item lg={8}>
                                    <div style={{ border: '1px solid #009D93', borderRadius: '5px', padding: '10px' }}>
                                        <InputLabel className='poppins_semi' style={{ color: "#494949", fontSize: "15px", marginBottom: '10px' }}>SMS sending history </InputLabel>
                                        <TableContainer>
                                            <Table size='small' aria-label="simple table">
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell className='poppins_semi' style={{ color: '#009D93', paddingLeft: '0' }} align="left">SMS CONTENT</TableCell>
                                                        <TableCell className='poppins_semi' style={{ color: '#009D93' }} align="left">TIME STAMP</TableCell>
                                                        <TableCell className='poppins_semi' style={{ color: '#009D93' }} align="left">DELIVERY STATUS</TableCell>
                                                        <TableCell className='poppins_semi' style={{ color: '#009D93' }} align="left">AGENT</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {sms_history_data.map((row) => (
                                                        <TableRow
                                                            key={row.content}
                                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                        >
                                                            <TableCell component="th" scope="row" style={{ paddingLeft: '0' }}>
                                                                {row.content}
                                                            </TableCell>
                                                            <TableCell component="th" scope="row">
                                                                {row.time_stamp}
                                                            </TableCell>
                                                            <TableCell component="th" scope="row">
                                                                {row.delivery_status}
                                                            </TableCell>
                                                            <TableCell component="th" scope="row">
                                                                {row.agent}
                                                            </TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                                <PaginationRounded />
                                            </Table>
                                        </TableContainer>
                                    </div>
                                </Grid>
                            </Grid>
                        </TabPanel>
                    </div>
                </div>

                <div className='white_div m-15'>
                    <Grid container rowSpacing={3} columnSpacing={2}>
                        <Grid item lg={8}>
                            <InputLabel className='poppins_semi' style={{ color: '#494949', padding: '10px 0', fontSize: '22px' }}>Call status</InputLabel>
                        </Grid>
                        <Grid item lg={4}>
                            <Grid container columnSpacing={2}>
                                <Grid item lg={6}>
                                    <Button variant="outlined" size='large' fullWidth className='urban_money_outlined_btn'>CALL STATUS LOG</Button>
                                </Grid>
                                <Grid item lg={6}>
                                    <Button variant="outlined" size='large' fullWidth className='urban_money_outlined_btn'>VIEW CHANGE LOG</Button>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item lg={3}>
                            <FormControl fullWidth size='small'>
                                <InputLabel color='success'>Category</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    label="Category"
                                    color='success'
                                >
                                    <MenuItem value={10}>L1</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item lg={3}>
                            <FormControl fullWidth size='small'>
                                <InputLabel color='success'>Sub Category</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    label="Category"
                                    color='success'
                                >
                                    <MenuItem value={10}>L2</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item lg={3}>
                            <FormControl fullWidth size='small'>
                                <InputLabel color='success'>Sub Sub Category</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    label="Category"
                                    color='success'
                                >
                                    <MenuItem value={10}>L3</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item lg={3}>
                            <Button variant="contained" className='urban_money_btn m-0' fullWidth>SUBMIT</Button>
                        </Grid>
                    </Grid>
                </div>

                <div className='white_div' style={{margin: '15px'}}>
                    <InputLabel className='poppins_semi' style={{color: '#494949', fontSize: '22px'}}> Information </InputLabel>

                    <Grid container rowSpacing={2} columnSpacing={2}>
                        <Grid item lg={3}></Grid>
                        <Grid item lg={3}></Grid>
                        <Grid item lg={3}></Grid>
                        <Grid item lg={3}></Grid>
                    </Grid>
                </div>
            </div>

            <Modal type={type} open={open} setOpen={setOpen} />
        </div>
    )
}

export default DetailView