const actions = {
  INCREMENT: 'auth/INCREMENT',
  DECREMENT: 'auth/DECREMENT',
  SET_ACCESSSTOKEN: 'auth/SET_ACCESSSTOKEN',
  SET_DATA: "auth/SET_DATA",
  SELECTEDMENU: "auth/SELECTEDMENU",
  SELECTEDHEADER: "auth/SELECTEDHEADER",

  setAccessToken: (accessToken) => (dispatch) => dispatch({
    type: actions.SET_ACCESSSTOKEN,
    accessToken,
  }),

  setUserData: (data) => {
    console.log("data=====>> ", data);
    let uData = {};
    if (data !== undefined && data !== null && Object.keys(data).length > 0) {
      uData = data;
    }

    return (dispatch) =>
      dispatch({
        type: actions.SET_DATA,
        userData: uData,
      });
  },
  setIncrement: (counter) => (dispatch) =>
    dispatch({
      type: actions.INCREMENT,
      counter: counter,
    }),
  setDecrement: (counter) => (dispatch) =>
    dispatch({
      type: actions.DECREMENT,
      counter: counter,
    }),
  setSelectedMenu: (menu) => (dispatch) =>
    dispatch({
      type: actions.SELECTEDMENU,
      selectedMenu: menu,
    }),
  setSelectedHeader: (header) => (dispatch) =>
    dispatch({
      type: actions.SELECTEDHEADER,
      selectedHeader: header,
    }),
};

export default actions;
