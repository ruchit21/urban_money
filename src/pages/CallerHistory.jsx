import { TextField } from "@mui/material";
import React from "react";
import {
  Button,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import SearchBar from "material-ui-search-bar";
import Data from "../Component/Data";



const CallerHistory = (props) => {
  return (
    <div style={{ width: "100%", margin: "10px 0 10px 10px" }}>
      <div className="urban_money_page_content">
        <div className="urban_money_page_title">
          <h2 style={{ margin: "0" }}>
            <b> {props?.routeName} </b>
          </h2>
        </div>
        <div style={{ padding: "20px" }}>
          <Grid container columnSpacing={2}>
            <Grid item lg={4}>
              <form noValidate>
                <TextField
                  id="date"
                  label="From date"
                  type="date"
                  fullWidth
                  placeholder="Start Date"
                  color="success"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </form>
            </Grid>
            <Grid item lg={4}>
              <form noValidate>
                <TextField
                  label="To date"
                  fullWidth
                  type="date"
                  placeholder="Start Date"
                  color="success"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </form>
            </Grid>
            <Grid item lg={4}>
              <SearchBar
                placeholder="Enter Keyword"
                fullWidth
                style={{
                  border: "1px solid #009D93",
                  height: "54px",
                  fontFamily: 'PoppinsRegular'
                }}
              />
            </Grid>
          </Grid>
        </div>
        <div style={{ padding: "10px 20px" }}>
          <div className="mb-15">
            <InputLabel className='poppins_semi'>Allocated to me:</InputLabel>
          </div>
          <Grid container columnSpacing={2}>
            <Grid item lg={4}>
              <Button
                fullWidth
                variant="outlined"
                className="urban_money_filter_btn_color poppins_regular"
              >
                Total actioned: <InputLabel className='urban_money_outlined_label poppins_semi'>90</InputLabel>
              </Button>
            </Grid>
            <Grid item lg={4}>
              <Button
                fullWidth
                variant="outlined"
                className="urban_money_filter_btn_color poppins_regular"
              >
                Rejected: <InputLabel className='urban_money_outlined_label poppins_semi'>30</InputLabel>
              </Button>
            </Grid>
            <Grid item lg={4}>
              <Button
                fullWidth
                variant="outlined"
                className="urban_money_filter_btn_color poppins_regular"
              >
                Forwarded to Approver: <InputLabel className='urban_money_outlined_label poppins_semi'>60</InputLabel>
              </Button>
            </Grid>
          </Grid>
        </div>
        <Data />
        <div style={{ padding: "20px" }}>
          <Grid container columnSpacing={2}>
            <Grid item lg={4}>
              <form noValidate>
                <TextField
                  id="date"
                  label="From date"
                  type="date"
                  fullWidth
                  placeholder="Start Date"
                  color="success"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </form>
            </Grid>
            <Grid item lg={4}>
              <form noValidate>
                <TextField
                  label="To date"
                  fullWidth
                  type="date"
                  placeholder="Start Date"
                  color="success"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </form>
            </Grid>
            <Grid item lg={4}>
              <SearchBar
                placeholder="Enter Keyword"
                fullWidth
                style={{
                  border: "1px solid #009D93",
                  height: "54px",
                  fontFamily: 'PoppinsRegular'
                }}
              />
            </Grid>
          </Grid>
        </div>
        <div style={{ padding: "10px 20px" }}>
          <div className="mb-15">
            <InputLabel className='poppins_semi'>Allocated to me:</InputLabel>
          </div>
          <Grid container columnSpacing={2}>
            <Grid item lg={2.4}>
              <Button
                fullWidth
                variant="contained"
                className="urban_money_filter_btn_color poppins_regular"
              >
                Total: <InputLabel className='urban_money_contained_label poppins_semi'>60</InputLabel>
              </Button>
            </Grid>
            <Grid item lg={2.4}>
              <Button
                fullWidth
                variant="outlined"
                className="urban_money_filter_btn_color poppins_regular"
              >
                Back to Caller(Approved): <InputLabel className='urban_money_outlined_label poppins_semi'>10</InputLabel>
              </Button>
            </Grid>
            <Grid item lg={2.4}>
              <Button
                fullWidth
                variant="outlined"
                className="urban_money_filter_btn_color poppins_regular"
              >
                Back to Caller(Rejected): <InputLabel className='urban_money_outlined_label poppins_semi'>2</InputLabel>
              </Button>
            </Grid>
            <Grid item lg={2.4}>
              <Button
                fullWidth
                variant="outlined"
                className="urban_money_filter_btn_color poppins_regular"
              >
                Approved: <InputLabel className='urban_money_outlined_label poppins_semi'>10</InputLabel>
              </Button>
            </Grid>
            <Grid item lg={2.4}>
              <Button
                fullWidth
                variant="outlined"
                className="urban_money_filter_btn_color poppins_regular"
              >
                Rejected: <InputLabel className='urban_money_outlined_label poppins_semi'>35</InputLabel>
              </Button>
            </Grid>
          </Grid>
        </div>
        <Data />
      </div>
    </div>
  )
}

export default CallerHistory