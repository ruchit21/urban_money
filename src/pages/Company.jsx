import { Pagination, Stack, TextField } from "@mui/material";
import React from "react";
import {
  Button,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import SearchBar from "material-ui-search-bar";
import Data from "../Component/Data";
import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import RemoveRedEyeOutlinedIcon from "@mui/icons-material/RemoveRedEyeOutlined";
import BorderColorOutlinedIcon from "@mui/icons-material/BorderColorOutlined";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

function createData(request, request_t, current_d, target_d, status) {
  return { request, request_t, current_d, target_d, status };
}

const rows = [
  createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
  createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
  createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
  createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
  
];

const Company = (props) => {
  return (
    <div style={{ width: "100%", margin: "10px 0 10px 10px" }}>
      <div className="urban_money_page_content">
        <div className="urban_money_page_title">
          <h2 style={{ margin: "0" }}>
            <b> {props?.routeName} </b>
          </h2>
        </div>
        <div style={{ padding: "20px" }}>
          <Grid container columnSpacing={2}>
            <Grid item lg={4}>
              <Grid container>
                <Grid item lg={8}>
                  <FormControl fullWidth>
                  <InputLabel style={{ fontFamily: 'PoppinsRegular' }} >
                      400-500
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="400-500"
                      color="success"
                    >
                      <MenuItem value={100}>0-100</MenuItem>
                      <MenuItem value={200}>100-200</MenuItem>
                      <MenuItem value={300}>200-300</MenuItem>
                      <MenuItem value={400}>300-400</MenuItem>
                      <MenuItem value={500}>400-500</MenuItem>
                      <MenuItem value={1000}>More Then 500</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item lg={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    className="urban_money_filter_btn"
                    style={{ fontFamily: "PoppinsRegular" }}
                  >
                    NO.OF EMPLOYEE
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <Grid item lg={4}>
              <form noValidate>
                <SearchBar
                  placeholder="Search..."
                  fullWidth
                  style={{
                    border: "1px solid #009D93",
                    height: "54px",
                    fontFamily: "PoppinsRegular",
                  }}
                />
              </form>
            </Grid>
            <Grid item lg={4}>
              <Button
                fullWidth
                variant="contained"
                className="urban_money_filter_btn"
                style={{ fontFamily: "PoppinsRegular" }}
              >
                <AddOutlinedIcon /> ADD
              </Button>
            </Grid>
          </Grid>
        </div>

        <div
          style={{
            margin: "20px",
            boxShadow: "0 .5rem 1rem rgba(0,0,0,.15)",
            borderRadius: "5px",
          }}
        >
          <TableContainer component={Paper}>
            <Table
              sx={{ minWidth: 650 }}
              size="small"
              aria-label="a dense table"
            >
              <TableHead>
                <TableRow>
                  <TableCell
                    style={{
                      color: "#009D93",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    NAME
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      color: "#009D93",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    NUMBER OF EMPLOYEE
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      color: "#009D93",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    WEBSITE
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      color: "#009D93",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    TENTATIVE SALARY DATE
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      color: "#009D93",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    ACTION
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map((row) => (
                  <TableRow
                    key={row.name}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.request}
                    </TableCell>
                    <TableCell align="center">{row.request_t}</TableCell>
                    <TableCell align="center">{row.current_d}</TableCell>
                    <TableCell align="center">{row.target_d}</TableCell>
                    <TableCell align="center">
                      <RemoveRedEyeOutlinedIcon
                        style={{ margin: "5px", color: "#009D93" }}
                      />
                      <BorderColorOutlinedIcon
                        style={{ margin: "5px", color: "#009D93" }}
                      />
                      <DeleteOutlinedIcon
                        style={{ margin: "5px", color: "red" }}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <Stack spacing={2}>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                padding: "10px 20px 20px 20px",
              }}
            >
              <Pagination
                count={2}
                variant="outlined"
                shape="rounded"
                color="success"
              />

              <FormControl>
                <InputLabel>10</InputLabel>
                <Select
                  // value={age}
                  label="10"
                  size="small"
                  color="success"
                  //   style={{ minWidth: '200px' }}
                >
                  <MenuItem value={10}>10</MenuItem>
                  <MenuItem value={20}>20</MenuItem>
                  <MenuItem value={30}>30</MenuItem>
                </Select>
              </FormControl>
            </div>
          </Stack>
        </div>
      </div>
    </div>
  );
};

export default Company;
