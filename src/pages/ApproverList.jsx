import React from 'react';
import SearchBar from "material-ui-search-bar";
import {
    Button,
    Grid,
    InputLabel,
} from "@mui/material";
import Data from "../Component/Data";

const ApproverList = (props) => {
    return (
        <div style={{ width: "100%", margin: "10px 0 10px 10px" }}>
            <div className="urban_money_page_content">
                <div className="urban_money_page_title">
                    <h2 style={{ margin: "0" }}>
                        <b className='poppins_semi'> {props?.routeName === "" ? "List" : "Approver List"} </b>
                    </h2>
                </div>
                <div
                    style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "space-between",
                        padding: "20px",
                    }}
                >
                    <InputLabel className='poppins_semi'>Allocated to me :</InputLabel>
                    <SearchBar
                        placeholder="Enter Keyword"
                        style={{
                            border: "1px solid #009D93",
                            minWidth: "512px",
                            height: "54px"
                        }}
                    />
                </div>

                <div style={{ padding: "20px" }}>
                    <Grid container columnSpacing={2}>
                        <Grid item lg={3}>
                            <Button
                                fullWidth
                                variant="contained"
                                className="urban_money_filter_btn_color poppins_regular"

                            >
                                Total: <InputLabel className='urban_money_contained_label poppins_semi'>80</InputLabel>
                            </Button>
                        </Grid>
                        <Grid item lg={3}>
                            <Button
                                fullWidth
                                variant="outlined"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Checked: <InputLabel className='urban_money_outlined_label poppins_semi'>20</InputLabel>
                            </Button>
                        </Grid>
                        <Grid item lg={3}>
                            <Button
                                fullWidth
                                variant="outlined"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Sent to Celler: <InputLabel className='urban_money_outlined_label poppins_semi'>21</InputLabel>
                            </Button>
                        </Grid>
                        <Grid item lg={3}>
                            <Button
                                fullWidth
                                variant="outlined"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Completed: <InputLabel className='urban_money_outlined_label poppins_semi'>9</InputLabel>
                            </Button>
                        </Grid>
                    </Grid>
                    <Grid container columnSpacing={2} sx={{ my: 2 }}>
                        <Grid item lg={3}>
                            <Button
                                fullWidth
                                variant="outlined"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Sent for Calling: <InputLabel className='urban_money_outlined_label poppins_semi'>4</InputLabel>
                            </Button>
                        </Grid>
                        <Grid item lg={3}>
                            <Button
                                fullWidth
                                variant="outlined"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Approved: <InputLabel className='urban_money_outlined_label poppins_semi'>18</InputLabel>
                            </Button>
                        </Grid>
                        <Grid item lg={3}>
                            <Button
                                fullWidth
                                variant="outlined"
                                className="urban_money_filter_btn_color poppins_regular"
                            >
                                Rejected: <InputLabel className='urban_money_outlined_label poppins_semi'>6</InputLabel>
                            </Button>
                        </Grid>
                        <Grid item lg={3}>

                        </Grid>
                    </Grid>
                </div>
                <Data />
            </div>
        </div>
    )
}

export default ApproverList