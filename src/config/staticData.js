import { Tablet } from '@material-ui/icons';
import Images from './images';
import MdOutlineDashboard from 'react-icons/md';

// TermsCondition Arayy
export const termsConditionArray = [
  {
    id: 1,
    title: "Welcome to Project",
    description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic"
  },
  {
    id: 2,
    title: "1. What are Terms and Conditions",
    description: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de FinibusBonorum et Malorum' (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance."
  },
  {
    id: 3,
    title: "A. What to include in a Terms and Conditions",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla"
  },
  {
    id: 4,
    title: "B. Is a Terms and Conditions required?",
    description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."
  },
  {
    id: 5,
    title: "2. Examples of Terms and Conditions",
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo"
  },
  {
    id: 6,
    title: "3. Your content in our services",
    description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."
  },
];

// PrivacyPolicy Arayy
export const privacyPolicyArray = [
  {
    id: 1,
    title: "Introduction",
    description: "Sed non dui aliquam, ullamcorper est non, aliquet mauris. Quisque lacus ligula, dapibus nec dignissim non, tincidunt vel quam. Etiam porttitor iaculis odio. Cras sagittis nunc lectus, in condimentum ligula ornare at. Etiam sagittis malesuada nisl. Duis volutpat ligula ante. Sed cursus, tortor a pellentesque pulvinar, lorem ipsum gravida elit, id posuere elit neque in est. Mauris ex justo, tincidunt at suscipit quis, pretium a mi."
  },
  {
    id: 2,
    title: "Using our services",
    description: "Sed luctus tristique est, non tempor ipsum tincidunt in. Vestibulum commodo eu augue nec maximus. Vestibulum vel metus varius turpis finibus accumsan in eu ligula. Phasellus semper ornare orci, sit amet fermentum neque faucibus id. Vestibulum id ante massa."
  },
  {
    id: 3,
    title: "Privacy and copyright protection",
    description: "Duis ultrices augue id feugiat commodo. Vivamus eu elit convallis, posuere neque at, ultricies tortor. Donec eleifend tortor vel laoreet convallis. Aliquam fermentum velit nunc, quis tristique neque imperdiet in. Etiam pretium fermentum eros vitae scelerisque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam dignissim urna eget dictum congue."
  },
  {
    id: 4,
    title: "Your content in our services",
    description: "Aenean id placerat tortor. Ut vel interdum odio. Mauris vitae tortor augue. Sed tincidunt, metus id venenatis dictum, nisi felis suscipit neque, pharetra consectetur nibh eros et lorem. Suspendisse potenti. Vivamus eu massa gravida, facilisis mauris vel, sodales diam. Phasellus at nisi condimentum, bibendum lacus eu, suscipit erat. Mauris fringilla laoreet tristique. Sed eleifend tincidunt erat, in cursus felis fringilla sit amet. Morbi commodo est dapibus metus tincidunt pellentesque."
  },
  {
    id: 5,
    title: "2. Examples of Privacy Policy",
    description: "Proin commodo aliquam elit eu dignissim. Pellentesque id maximus elit. Nunc ultrices rutrum odio, ut pulvinar erat ultrices vitae. Nunc nec odio eget lacus vehicula suscipit ac in elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer sit amet tincidunt ante. Aliquam scelerisque dictum tortor, nec tristique augue luctus at. Nulla vel bibendum eros. Ut efficitur, sapien ac maximus fringilla, erat nunc aliquam dui, auctor commodo lacus ipsum ac sapien. Nullam a ex vitae tellus congue consectetur consequat non velit. Maecenas lobortis sapien eget tellus blandit congue. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam gravida commodo justo, ut fermentum sapien lobortis et."
  },
  {
    id: 6,
    title: "3. Your content in our services",
    description: "Integer vel dictum felis, vitae gravida sem. Suspendisse quis auctor nibh. Quisque accumsan lectus dictum, efficitur magna eu, malesuada lacus. Aliquam fermentum, justo eget consequat maximus, nunc purus posuere ex, ac scelerisque ante est nec nibh. Morbi scelerisque imperdiet mi, in cursus tortor sollicitudin ac. Praesent eget congue augue. Mauris facilisis a arcu vitae dictum. Vivamus quis tellus ut urna commodo porttitor a vel magna"
  },
];

// About Arayy
export const aboutArray = [
  {
    id: 1,
    title: "Welcome to Project",
    description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic"
  },
  {
    id: 2,
    title: "1. What are Terms and Conditions",
    description: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de FinibusBonorum et Malorum' (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance."
  },
  {
    id: 3,
    title: "A. What to include in a Terms and Conditions",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla"
  },
  {
    id: 4,
    title: "B. Is a Terms and Conditions required?",
    description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."
  },
  {
    id: 5,
    title: "2. Examples of Terms and Conditions",
    description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo"
  },
  {
    id: 6,
    title: "3. Your content in our services",
    description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."
  },
];
// stausbar main array
export const StatusbarArray = [
  {
    id: 1,
    title: 'Privacy Policy',
    link: '/privacypolicy',
    selected: true,

  },
  {
    id: 2,
    title: 'Terms & Condition',
    link: '/termscondition',
    selected: false,

  },
  {
    id: 3,
    title: 'About',
    link: '/about',
    selected: false,

  },
  {
    id: 4,
    title: 'Contact Us',
    link: '/contactus',
    selected: false,

  },
];

export const coursesArray = [

  {
    id: 1,
    title: 'My courses',
    link: '/moreAboutUs',
    selected: true,
  },

  {
    id: 2,
    title: 'All courses',
    link: '/moreAboutUs',
    selected: true,
  },
];

export const MycourceArray = [

  {
    id: 1,
    img: require('../config/assets/images/study.jpg'),
    title: 'Advance Mathematics Grade 1',
    link: '/moreAboutUs',
  },

  {
    id: 2,
    img: require('../config/assets/images/study.jpg'),
    title: 'Advance Mathematics Grade 2',
    link: '/moreAboutUs',
  },
  {
    id: 3,
    img: require('../config/assets/images/study.jpg'),
    title: 'Advance Mathematics Grade 3',
    link: '/moreAboutUs',
  },
];

export const MenuArray = [
  {
    id: 1,
    name: 'Dashboard',
    screen: '/dashboard'
  },
  {
    id: 2,
    name: 'Student Management',
    screen: '/studentmanagement'
  },
  {
    id: 3,
    name: 'Course Management',
    screen: '/coursemanagement'
  },
  {
    id:4,
    name: 'Company Profile',
    screen: '/companyprofile'
  },
  {
    id: 5,
    name: 'Logout',
  }
];

// Student Management Table Modal 
export const tableSearchModal = [
  {
    id: 0,
    title: "Search Name",
    placeholder: "Enter Name"
  },
  {
    id: 1,
    title: "Search Registered",
    placeholder: "Enter Registered"
  },
  {
    id: 2,
    title: "Search Registered Email",
    placeholder: "Enter Registered Email"
  },
]

// more subCatagory Array testingggggg

export const Morecatagory = [
  {
    id: 1,
    subtitle: 'About Us',
    link: '/moreAboutUs',
  },
  {
    id: 2,
    subtitle: 'Agent Login',
    link: '',
  },
  {
    id: 3,
    subtitle: 'Careers',
    link: '/moreCareer',
  },
  {
    id: 4,
    subtitle: 'Contact Us',
    link: 'moreContactUs'
  },
];
export const CourseInfoArray = [
  {
    id: 1,
    title: 'Business Management',
    width: '60%',
  },
  {
    id: 2,
    title: 'Health & Social Care',
    width: '95%',
  },
  {
    id: 3,
    title: 'Construction & Trade Skills',
    width: '45%',
  },
  {
    id: 4,
    title: 'Fitness Diet & Nutrition',
    width: '55%',
  }
];
// 

export const TableData = [
  {
    id: 1,
    name: "William Zabka",
    registered: '04/04/2022|06:48',
    email: "williamzabka12@gmail.com",
    status: "Enrolled in 1 course",

  },
  {
    id: 2,
    name: "William Zabka",
    registered: '04/04/2022|06:48',
    email: "williamzabka12@gmail.com",
    status: "Enrolled in 1 course",

  },
  {
    id: 3,
    name: "William Zabka",
    registered: '04/04/2022|06:48',
    email: "williamzabka12@gmail.com",
    status: "Enrolled in 1 course",

  },
  {
    id: 4,
    name: "William Zabka",
    registered: '04/04/2022|06:48',
    email: "williamzabka12@gmail.com",
    status: "Enrolled in 1 course",

  },
  {
    id: 5,
    name: "William Zabka",
    registered: '04/04/2022|06:48',
    email: "williamzabka12@gmail.com",
    status: "Enrolled in 1 course",

  },
  {
    id: 6,
    name: "William Zabka",
    registered: '04/04/2022|06:48',
    email: "williamzabka12@gmail.com",
    status: "Enrolled in 1 course",

  },
];

export const pendingInvite = [
  {
    id: 1,
    email: "xyz@gmail.com",
    status: 'pending'
  },
  {
    id: 2,
    email: "xyz@gmail.com",
    status: 'pending'
  },
  {
    id: 3,
    email: "xyz@gmail.com",
    status: 'pending'
  },
  {
    id: 4,
    email: "xyz@gmail.com",
    status: 'pending'
  },
  {
    id: 5,
    email: "xyz@gmail.com",
    status: 'pending'
  },
  {
    id: 6,
    email: "xyz@gmail.com",
    status: 'pending'
  },
  {
    id: 7,
    email: "xyz@gmail.com",
    status: 'pending'
  },
  {
    id: 8,
    email: "xyz@gmail.com",
    status: 'pending'
  },
  {
    id: 9,
    email: "xyz@gmail.com",
    status: 'pending'
  },
  {
    id: 10,
    email: "xyz@gmail.com",
    status: 'pending'
  },
  {
    id: 11,
    email: "xyz@gmail.com",
    status: 'pending'
  },
];

export const newUser = [
  {
    id: 1,
    name: "Xyz",
    email: "xyz@gmail.com",
    status: "Enrolled in 1 course"
  },
  {
    id: 2,
    name: "Xyz",
    email: "xyz@gmail.com",
    status: "Enrolled in 2 course"
  },
  {
    id: 3,
    name: "Xyz",
    email: "xyz@gmail.com",
    status: "Enrolled in 4 course"
  },
];

export const ActionArray = [
  {
    id: 1,
    chapter: 'Chapter 1',
    status: 'Completed'
  },
  {
    id: 2,
    chapter: 'Chapter 2',
    status: 'Completed'
  },
  {
    id: 3,
    chapter: 'Chapter 3',
    status: 'Completed'
  },
  {
    id: 4,
    chapter: 'Chapter 4',
    status: 'Pending'
  },
];

export const carFaqData = [
  {
    id: 1,
    question: 'Chapter 1',
    answer:
      'Car Hire Excess  between car hire providers and normally ranges between €500 and €2,000.',
  },
  {
    id: 2,
    question: 'Chapter 2',
    answer:
      'This policy operates on have to pay (up to the policy limit) under the terms of the rental agreement following damage to the rental vehicle.',
  },
  {
    id: 3,
    question: 'Chapter 3',
    answer:
      'Damage to the renta accident and theft and also loss of use of the rental vehicle is covered.',
  },
  {
    id: 4,
    question: 'Chapter 4',
    answer:
      'Yes, this policy alsaximum of €2,000 in a/key for a rental vehicle, including replacement locks and locksmith charges.',
  },
  {
    id: 5,
    question: 'Chapter 5',
    answer:
      'No excess applies to this policy, the amount you up to the limit specified in the policy.',
  }
];

export const sizeOfTeamArray = [
  {
    label: '12'
  },
  {
    label: '11'
  },
  {
    label: '11'
  },
  {
    label: '11'
  },
  {
    label: '11'
  },
];

export const CourseManagementArray = [
  {
    id: 1,
    image: require('../config/assets/images/study.jpg'),
    title: 'Advance Mathematics Grade 1',
    enrollname: 'Student enrolled',
    enrollNo: 50
  },
  {
    id: 2,
    image: require('../config/assets/images/bookimage.jpg'),
    title: 'Advance Mathematics Grade 1',
    enrollname: 'Student enrolled',
    enrollNo: 40
  },
  {
    id: 3,
    image: require('../config/assets/images/groupstudy.jpeg'),
    title: 'Advance Mathematics Grade 1',
    enrollname: 'Student enrolled',
    enrollNo: 50
  },
  {
    id: 4,
    image: require('../config/assets/images/singlestudy.jpeg'),
    title: 'Advance Mathematics Grade 1',
    enrollname: 'Student enrolled',
    enrollNo: 30
  },
  {
    id: 5,
    image: require('../config/assets/images/study1.jpeg'),
    title: 'Advance Mathematics Grade 1',
    enrollname: 'Student enrolled',
    enrollNo: 50
  },
  {
    id: 6,
    image: require('../config/assets/images/studyroom.jpg'),
    title: 'Advance Mathematics Grade 1',
    enrollname: 'Student enrolled',
    enrollNo: 60
  },
];
