/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

const withDirection = (Component) => (props) => {
  let direction = 'ltr';

  if (typeof window !== 'undefined') {
    direction = document.getElementsByTagName('html')[0].getAttribute('dir');
  }

  return <Component {...props} data-rtl={direction} />;
};

export default withDirection;
