import React, { useEffect, useState, Suspense } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Routes,
  Redirect,
  useNavigate,
  useLocation
} from "react-router-dom";
import { color } from "../src/config/theme";
import { CircularProgress } from "@mui/material";
// import Login from './pages/Login/index';
// import Home from './pages/Home/index';
import { useDispatch, useSelector } from "react-redux";
import authActions from "../src/redux/reducers/auth/actions";
import siteConfig from "./config/siteConfig";
// import {useNavigate} from "react-router-dom";
import _ from 'lodash';
import { windowHeight } from "./config/dimensions";
import Login from "./pages/Login";
import './App.css'
import Sidebar from "./Component/Sidebar";
import List from "./pages/List";
import History from "./pages/History";
import DetailView from "./pages/DetailView";
import ApproverDetailsView from "./pages/ApproverDetailsView";
import Company from "./pages/Company";
import AddCompany from "./pages/AddCompany";
import College from "./pages/CollegeList"
import AddCollege from "./pages/AddCollege";
import CallerList from "./pages/CallerList";
import ApproverList from "./pages/ApproverList";
import CallerHistory from "./pages/CallerHistory";
import ApproverHistory from "./pages/ApproverHistory";

export default function App() {
  const auth = useSelector((state) => state.auth);
  const token = localStorage.getItem('token');
  const userData = useSelector(state => state.auth.userData);
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();
  const location = useLocation();
  let path = location.pathname.split("/", 2);

  useEffect(() => {
    if (token === null) navigate("/login");
    // eslint-disable-next-line
  }, [token, navigate]);

  return (
    <Suspense>
      <div style={{ height: '100%' }}>
        <div style={{ display: 'flex', backgroundColor: '#DFFEFC', height: '100%' }}>
          {token !== null && path[1] !== 'detail-view' &&  path[1] !== 'approver-details-view' ?  <Sidebar /> : ''}
          <Routes>
            <Route exact path="/" element={<List routeName={(path[1].replaceAll('-', ' ')).toUpperCase()} />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/history" element={<History routeName={(path[1].replaceAll('-', ' ')).toUpperCase()} />} />
            <Route exact path="/company" element={<Company routeName={(path[1].replaceAll('-', ' ')).toUpperCase()} />} />
            <Route exact path="/add-company" element={<AddCompany routeName={(path[1].replaceAll('-', ' ')).toUpperCase()} />} />
            <Route exact path="/detail-view" element={<DetailView routeName={(path[1].replaceAll('-', ' ')).toUpperCase()} />} />
            <Route exact path="/approver-details-view" element={<ApproverDetailsView routeName = {(path[1].replaceAll('-', ' ')).toUpperCase()}/>} />
            <Route exact path="/college" element={<College routeName={(path[1].replaceAll('-', ' ')).toUpperCase()} />} />
            <Route exact path="/college/add-college" element={<AddCollege  routeName={(path[1].replaceAll('-', ' ')).toUpperCase()} />} />
            <Route exact path="/caller-list" element={<CallerList  routeName={(path[1].replaceAll('-', ' ')).toUpperCase()} />} />
            <Route exact path="/approver-list" element={<ApproverList  routeName={(path[1].replaceAll('-', ' ')).toUpperCase()} />} /> 
            <Route exact path="/caller-history" element={<CallerHistory  routeName={(path[1].replaceAll('-', ' ')).toUpperCase()} />} />
            <Route exact path="/approver-history" element={<ApproverHistory  routeName={(path[1].replaceAll('-', ' ')).toUpperCase()} />} />
          </Routes>
        </div>
      </div>
    </Suspense>
  );
}
