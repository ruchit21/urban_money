import { Box, FormControl, Grid, InputLabel, MenuItem, Modal, Select, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from '@mui/material'
import React, { useState } from 'react'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined';
import { Button } from '@material-ui/core';
import { FiUpload } from 'react-icons/fi';
import _ from 'lodash'
import Images from '../config/images';
import { styled } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import AddIcon from '@material-ui/icons/Add';

const Input = styled('input')({
    display: 'none',
});

const { user } = Images;

const Modals = (props) => {

    const handleClose = () => {
        props?.setOpen(false);
    }

    function historyData(number, type, call_type, response, call_duration, agent_details, time_stamp, remark) {
        return { number, type, call_type, response, call_duration, agent_details, time_stamp, remark };
    }

    function changeLogData(timestamp, date_changed, change_by) {
        return { timestamp, date_changed, change_by };
    }

    function statusLogData(timestamp, changed_by, l1, l2, l3) {
        return { timestamp, changed_by, l1, l2, l3 };
    }

    function matchFoundData(customer_id, name, matching_fields, percentege) {
        return { customer_id, name, matching_fields, percentege }
    }

    function viewDetailsData(kyc_type, kyc_id, data, timestamp_call, timestamp_data, name) {
        return { kyc_type, kyc_id, data, timestamp_call, timestamp_data, name }
    }

    const Callhistory = [
        historyData(9789797977, 'Registered', 'Dialer', 'Connected', '04:06', 'Piyush(CA7676)', '', ''),
        historyData(9789797977, 'Alternate', 'Click2Call', 'Busy', '00:46', 'Piyush(CA7676)', '', ''),
        historyData(9877889984, 'Custom', 'Schedules Call Back', 'Disconnected', '00:25', 'Piyush(CA7676)', '', '')
    ];

    const ChangedLog = [
        changeLogData('20-08-21 | 19:00', 'Remove KYC document', 'Kabir (Caller - AC5749)'),
        changeLogData('02-08-21 | 09:00', 'Update PAN ID', 'Jitendra (Approver - AA90744)'),
        changeLogData('...', '...', '...'),
        changeLogData('...', '...', '...'),
        changeLogData('...', '...', '...'),
    ];

    const StatusLog = [
        statusLogData('31-08-21 | 17:00', 'Krishna (Approver- AC5349)', '', '', ''),
        statusLogData('20-08-21 | 19:00', 'Kabir (Caller- AC5749)', '', '', ''),
        statusLogData('02-08-21 | 9:00', 'Jitendra (Approver- AA90744)', '', '', ''),
    ]

    const matchFound = [
        matchFoundData('12345', 'Rohan Mathur', 'KYC PAN', '40%'),
        matchFoundData('67877', 'Pritha Mathur', 'Email', '70%'),
        matchFoundData('67883', 'Rishi Pal', 'Phone Number', '80%'),
    ]

    const viewDetails = [
        viewDetailsData('', '', '', '', '', ''),
        viewDetailsData('', '', '', '', '', ''),
    ]

    const [image, setImage] = useState(null);

    const imageChange = (e) => {
        var url = e.target.value;
        var filename = url.substring(url.lastIndexOf('\\') + 1);
        setImage(filename);
    }

    return (
        <div>
            {
                props?.type === 'remove-image' ?
                    <Modal
                        open={props?.open}
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box className="urban_money_modal w-350">
                            <div className='urban_money_modal_title'>
                                <h3>Remove Image</h3>
                                <CloseOutlinedIcon onClick={handleClose} style={{ color: '#fff', cursor: 'pointer' }} />
                            </div>
                            <div className='urban_money_modal_content'>
                                <div className='mt-15'>
                                    <FormControl fullWidth>
                                        <InputLabel color='success' id="demo-simple-select-label">Select Reason</InputLabel>
                                        <Select
                                            label="Select Reason"
                                            color='success'
                                        >
                                            <MenuItem value={10}>Ten</MenuItem>
                                            <MenuItem value={20}>Twenty</MenuItem>
                                            <MenuItem value={30}>Thirty</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div>

                                <div className='mt-15'>
                                    <TextField type="text" label="Comment" variant="outlined" name="model_comment" placeholder="Enter Comment" fullWidth />
                                </div>

                                <Button variant="contained" className='urban_money_btn' fullWidth>Save</Button>
                            </div>
                        </Box>
                    </Modal> : props?.type === 'call-history' ?
                        <Modal
                            open={props?.open}
                            onClose={handleClose}
                            aria-labelledby="modal-modal-title"
                            aria-describedby="modal-modal-description"
                        >
                            <Box className="urban_money_modal w-100">
                                <div className='urban_money_modal_title'>
                                    <h3>Call History</h3>
                                    <CloseOutlinedIcon onClick={handleClose} style={{ color: '#fff', cursor: 'pointer' }} />
                                </div>
                                <div className='urban_money_modal_content'>
                                    <TableContainer>
                                        <Table aria-label="simple table">
                                            <TableHead>
                                                <TableRow>
                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> NUMBER </b> </TableCell>
                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> TYPE </b> </TableCell>
                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> CALL TYPE </b> </TableCell>
                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> RESPONSE </b> </TableCell>
                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> CALL DURATION </b> </TableCell>
                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> AGENT DETAILS </b> </TableCell>
                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> TIME STAMP </b> </TableCell>
                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> REMARK </b> </TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {Callhistory.map((row) => (
                                                    <TableRow
                                                        key={row.number}
                                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                    >
                                                        <TableCell component="th" scope="row" align='left'>
                                                            {row.number}
                                                        </TableCell>
                                                        <TableCell align="left">{row.type}</TableCell>
                                                        <TableCell align="left">{row.call_type}</TableCell>
                                                        <TableCell align="left">{row.response}</TableCell>
                                                        <TableCell align="left">{row.call_duration}</TableCell>
                                                        <TableCell align="left">{row.agent_details}</TableCell>
                                                        <TableCell align="left">{row.time_stamp}</TableCell>
                                                        <TableCell align="left">{row.remark}</TableCell>
                                                    </TableRow>
                                                ))}
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </div>
                            </Box>
                        </Modal>
                        : props?.type === 'view-change-log' ?
                            <Modal
                                open={props?.open}
                                onClose={handleClose}
                                aria-labelledby="modal-modal-title"
                                aria-describedby="modal-modal-description"
                            >
                                <Box className="urban_money_modal w-950">
                                    <div className='urban_money_modal_title'>
                                        <h3>View Change Log</h3>
                                        <CloseOutlinedIcon onClick={handleClose} style={{ color: '#fff', cursor: 'pointer' }} />
                                    </div>
                                    <div className='urban_money_modal_content'>
                                        <TableContainer>
                                            <Table aria-label="simple table">
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell align="left" style={{ color: '#009D93' }}> <b> TIMESTAMP </b> </TableCell>
                                                        <TableCell align="left" style={{ color: '#009D93' }}> <b> DATE CHANGED </b> </TableCell>
                                                        <TableCell align="left" style={{ color: '#009D93' }}> <b> CHANGED BY (ROLE-AGENT ID) </b> </TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {StatusLog.map((row) => (
                                                        <TableRow
                                                            key={row.timestamp}
                                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                        >
                                                            <TableCell component="th" scope="row" align='left'>
                                                                {row.timestamp}
                                                            </TableCell>
                                                            <TableCell align="left">{row.date_changed}</TableCell>
                                                            <TableCell align="left">{row.change_by}</TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </div>
                                </Box>
                            </Modal>
                            : props?.type === 'call-status-log' ?
                                <Modal
                                    open={props?.open}
                                    onClose={handleClose}
                                    aria-labelledby="modal-modal-title"
                                    aria-describedby="modal-modal-description"
                                >
                                    <Box className="urban_money_modal w-950">
                                        <div className='urban_money_modal_title'>
                                            <h3>Call Status Log</h3>
                                            <CloseOutlinedIcon onClick={handleClose} style={{ color: '#fff', cursor: 'pointer' }} />
                                        </div>
                                        <div className='urban_money_modal_content'>
                                            <TableContainer>
                                                <Table aria-label="simple table">
                                                    <TableHead>
                                                        <TableRow>
                                                            <TableCell align="left" style={{ color: '#009D93' }}> <b> TIMESTAMP </b> </TableCell>
                                                            <TableCell align="left" style={{ color: '#009D93' }}> <b> CHANGED BY </b> </TableCell>
                                                            <TableCell align="left" style={{ color: '#009D93' }}> <b> L1 </b> </TableCell>
                                                            <TableCell align="left" style={{ color: '#009D93' }}> <b> L2 </b> </TableCell>
                                                            <TableCell align="left" style={{ color: '#009D93' }}> <b> L3 </b> </TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                    <TableBody>
                                                        {ChangedLog.map((row) => (
                                                            <TableRow
                                                                key={row.timestamp}
                                                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                            >
                                                                <TableCell component="th" scope="row" align='left'>
                                                                    {row.timestamp}
                                                                </TableCell>
                                                                <TableCell align="left">{row.change_by}</TableCell>
                                                                <TableCell align="left">{row.l1}</TableCell>
                                                                <TableCell align="left">{row.l2}</TableCell>
                                                                <TableCell align="left">{row.l3}</TableCell>
                                                            </TableRow>
                                                        ))}
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </div>
                                    </Box>
                                </Modal>
                                : props?.type === 'match-found' ?
                                    <Modal
                                        open={props?.open}
                                        onClose={handleClose}
                                        aria-labelledby="modal-modal-title"
                                        aria-describedby="modal-modal-description"
                                    >
                                        <Box className="urban_money_modal w-950">
                                            <div className='urban_money_modal_title'>
                                                <h3>Match Found</h3>
                                                <CloseOutlinedIcon onClick={handleClose} style={{ color: '#fff', cursor: 'pointer' }} />
                                            </div>
                                            <div className='urban_money_modal_content'>
                                                <TableContainer>
                                                    <Table aria-label="simple table">
                                                        <TableHead>
                                                            <TableRow>
                                                                <TableCell align="left" style={{ color: '#009D93' }}> <b> CUSTOMER ID </b> </TableCell>
                                                                <TableCell align="left" style={{ color: '#009D93' }}> <b> NAME </b> </TableCell>
                                                                <TableCell align="left" style={{ color: '#009D93' }}> <b> MATCHING FIELDS </b> </TableCell>
                                                                <TableCell align="left" style={{ color: '#009D93' }}> <b> PERCENTAGE </b> </TableCell>
                                                            </TableRow>
                                                        </TableHead>
                                                        <TableBody>
                                                            {matchFound.map((row) => (
                                                                <TableRow
                                                                    key={row.customer_id}
                                                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                                >
                                                                    <TableCell component="th" scope="row" align='left'>
                                                                        {row.customer_id}
                                                                    </TableCell>
                                                                    <TableCell align="left">{row.name}</TableCell>
                                                                    <TableCell align="left">{row.matching_fields}</TableCell>
                                                                    <TableCell align="left">{row.percentege}</TableCell>
                                                                </TableRow>
                                                            ))}
                                                        </TableBody>
                                                    </Table>
                                                </TableContainer>
                                            </div>
                                        </Box>
                                    </Modal>
                                    : props?.type === 'view-details' ?
                                        <Modal
                                            open={props?.open}
                                            onClose={handleClose}
                                            aria-labelledby="modal-modal-title"
                                            aria-describedby="modal-modal-description"
                                        >
                                            <Box className="urban_money_modal w-100">
                                                <div className='urban_money_modal_title'>
                                                    <h3>View Details</h3>
                                                    <CloseOutlinedIcon onClick={handleClose} style={{ color: '#fff', cursor: 'pointer' }} />
                                                </div>
                                                <div className='urban_money_modal_content'>
                                                    <TableContainer>
                                                        <Table aria-label="simple table">
                                                            <TableHead>
                                                                <TableRow>
                                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> KYC TYPE </b> </TableCell>
                                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> KYC ID </b> </TableCell>
                                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> DATA </b> </TableCell>
                                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> TIME STAMP OF THE CALL </b> </TableCell>
                                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> TIME STAMP OF THE RETURN DATA </b> </TableCell>
                                                                    <TableCell align="left" style={{ color: '#009D93' }}> <b> AGENT NAME (ID) </b> </TableCell>
                                                                </TableRow>
                                                            </TableHead>
                                                            <TableBody>
                                                                {viewDetails.map((row) => (
                                                                    <TableRow
                                                                        key={row.kyc_type}
                                                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                                    >
                                                                        <TableCell component="th" scope="row" align='left'>
                                                                            {row.kyc_type}
                                                                        </TableCell>
                                                                        <TableCell align="left">{row.kyc_id}</TableCell>
                                                                        <TableCell align="left">{row.data}</TableCell>
                                                                        <TableCell align="left">{row.timestamp_call}</TableCell>
                                                                        <TableCell align="left">{row.timestamp_data}</TableCell>
                                                                        <TableCell align="left">{row.name}</TableCell>
                                                                    </TableRow>
                                                                ))}
                                                                <Button className='urban_money_btn' variant='contained' size='md'>Ok</Button>
                                                            </TableBody>
                                                        </Table>
                                                    </TableContainer>
                                                </div>
                                            </Box>
                                        </Modal>
                                        : props?.type === 'add-document' ?
                                            <Modal
                                                open={props?.open}
                                                onClose={handleClose}
                                                aria-labelledby="modal-modal-title"
                                                aria-describedby="modal-modal-description"
                                            >
                                                <Box className="urban_money_modal w-350">
                                                    <div className='urban_money_modal_title'>
                                                        <h3>Add Document type & comment</h3>
                                                        <CloseOutlinedIcon onClick={handleClose} style={{ color: '#fff', cursor: 'pointer' }} />
                                                    </div>
                                                    <div className='urban_money_modal_content'>
                                                        <div className='mt-15'>
                                                            <FormControl fullWidth>
                                                                <InputLabel color='success' id="demo-simple-select-label">Document Type</InputLabel>
                                                                <Select
                                                                    label="Document Type"
                                                                    color='success'
                                                                >
                                                                    <MenuItem value={10}>Ten</MenuItem>
                                                                    <MenuItem value={20}>Twenty</MenuItem>
                                                                    <MenuItem value={30}>Thirty</MenuItem>
                                                                </Select>
                                                            </FormControl>
                                                        </div>

                                                        <div className='mt-15'>
                                                            <TextField type="text" color='success' label="Comment" variant="outlined" placeholder="Enter Comment" fullWidth />
                                                        </div>

                                                        <div className='mt-15' style={{ position: 'relative' }}>
                                                            <TextField className='document_upload_modal' type="file" color='success' variant="outlined" placeholder="Select Image" fullWidth onChange={imageChange} />
                                                            {!_.isNull(image) ? <p style={{ position: 'absolute', top: '15px', left: '15px', fontSize: '15px', color: '#009D93', cursor: 'pointer' }}>{image}</p> : <p style={{ position: 'absolute', top: '15px', left: '15px', fontSize: '15px', color: '#009D93', cursor: 'pointer' }}> Upload Image </p>}
                                                            <FiUpload size={25} style={{ position: 'absolute', top: 15, right: 10, color: '#009D93' }} />
                                                        </div>

                                                        <Button variant="contained" className='urban_money_btn' fullWidth>Save</Button>
                                                    </div>
                                                </Box>
                                            </Modal>
                                            : props?.type === 'confirm' ?
                                                <Modal
                                                    open={props?.open}
                                                    onClose={handleClose}
                                                    aria-labelledby="modal-modal-title"
                                                    aria-describedby="modal-modal-description"
                                                >
                                                    <Box className="urban_money_modal w-300">
                                                        <div className='urban_money_modal_title'>
                                                            <h3 style={{ fontFamily: 'PoppinsMedium' }}>Confirmation</h3>
                                                            <CloseOutlinedIcon onClick={handleClose} style={{ color: '#fff', cursor: 'pointer' }} />
                                                        </div>
                                                        <div className='urban_money_modal_content'>
                                                            <div className='p-20'>
                                                                <p className='text-center' style={{ fontFamily: 'PoppinsMedium' }}>Are you sure?</p>
                                                                <Grid container columnSpacing={2} className="mt-15">
                                                                    <Grid item lg={6}>
                                                                        <Button style={{ fontFamily: 'PoppinsMedium' }} fullWidth variant='outlined' className='urban_money_outlined_btn'>NO</Button>
                                                                    </Grid>

                                                                    <Grid item lg={6}>
                                                                        <Button style={{ fontFamily: 'PoppinsMedium' }} fullWidth variant='contained' className='urban_money_btn m-0'>YES</Button>
                                                                    </Grid>
                                                                </Grid>
                                                            </div>
                                                        </div>
                                                    </Box>
                                                </Modal>
                                                : props?.type === 'disperancy' ?
                                                    <Modal
                                                        open={props?.open}
                                                        onClose={handleClose}
                                                        aria-labelledby="modal-modal-title"
                                                        aria-describedby="modal-modal-description"
                                                    >
                                                        <Box className="urban_money_modal w-300">
                                                            <div className='urban_money_modal_title'>
                                                                <h3 className='poppins_semi'>Disperancy</h3>
                                                                <CloseOutlinedIcon onClick={handleClose} style={{ color: '#fff', cursor: 'pointer' }} />
                                                            </div>
                                                            <div className='urban_money_modal_content'>
                                                                <div className='p-10'>
                                                                    <TextField
                                                                        label="Comment"
                                                                        multiline
                                                                        rows={4}
                                                                        color="success"
                                                                        fullWidth
                                                                    />

                                                                    <Button fullWidth variant='contained' className='urban_money_btn'>Save</Button>
                                                                </div>
                                                            </div>
                                                        </Box>
                                                    </Modal>
                                                    : props?.type === 'add-course-details' ?
                                                        <Modal
                                                            open={props?.open}
                                                            onClose={handleClose}
                                                            aria-labelledby="modal-modal-title"
                                                            aria-describedby="modal-modal-description"
                                                            style={{ overflow: 'scroll' }}
                                                        >
                                                            <Box className="urban_money_modal  w-950">
                                                                <div className='urban_money_modal_title'>
                                                                    <h3 className='poppins_semi'>Add new course details</h3>
                                                                    <CloseOutlinedIcon onClick={handleClose} style={{ color: '#fff', cursor: 'pointer' }} />
                                                                </div>
                                                                <div className='urban_money_modal_content'>
                                                                    <div style={{ padding: "15px" }}>
                                                                        <Grid container columnSpacing={2}>
                                                                            <Grid item lg={4}>
                                                                                <FormControl fullWidth>
                                                                                    <InputLabel className="poppins_regular" color="success">
                                                                                        Type of Degree
                                                                                    </InputLabel>
                                                                                    <Select
                                                                                        labelId="demo-simple-select-label"
                                                                                        id="demo-simple-select"
                                                                                        label="Type of Degree"
                                                                                        color="success"
                                                                                    >
                                                                                        <MenuItem value={10}>Master</MenuItem>
                                                                                        <MenuItem value={20}>Bachelor</MenuItem>
                                                                                        <MenuItem value={30}>Master</MenuItem>
                                                                                    </Select>
                                                                                </FormControl>
                                                                            </Grid>
                                                                            <Grid item lg={4}>
                                                                                <FormControl fullWidth>
                                                                                    <InputLabel className="poppins_regular" color="success">
                                                                                        Name of degree
                                                                                    </InputLabel>
                                                                                    <Select
                                                                                        labelId="demo-simple-select-label"
                                                                                        id="demo-simple-select"
                                                                                        label="Name of degree"
                                                                                        color="success"
                                                                                    >
                                                                                        <MenuItem value={10}>Msc</MenuItem>
                                                                                        <MenuItem value={20}>MCA</MenuItem>
                                                                                        <MenuItem value={30}>BCA</MenuItem>
                                                                                    </Select>
                                                                                </FormControl>
                                                                            </Grid>
                                                                            <Grid item lg={4}>
                                                                                <TextField id="outlined-basic" label="Specialization" variant="outlined" color="success" fullWidth placeholder='Enter Specialization' />
                                                                            </Grid>
                                                                        </Grid>
                                                                    </div>
                                                                    <div style={{ padding: "15px" }}>
                                                                        <Grid container columnSpacing={2}>
                                                                            <Grid item lg={4}>
                                                                                <FormControl fullWidth>
                                                                                    <InputLabel className="poppins_regular" color="success">
                                                                                        Type of attendance
                                                                                    </InputLabel>
                                                                                    <Select
                                                                                        labelId="demo-simple-select-label"
                                                                                        id="demo-simple-select"
                                                                                        label="Type of attendance"
                                                                                        color="success"
                                                                                    >
                                                                                        <MenuItem value={10}>Full time</MenuItem>
                                                                                        <MenuItem value={20}>Part time</MenuItem>
                                                                                        <MenuItem value={30}>time</MenuItem>
                                                                                    </Select>
                                                                                </FormControl>
                                                                            </Grid>
                                                                            <Grid item lg={4}>
                                                                                <FormControl fullWidth>
                                                                                    <InputLabel className="poppins_regular" color="success">
                                                                                        Course tenture
                                                                                    </InputLabel>
                                                                                    <Select
                                                                                        labelId="demo-simple-select-label"
                                                                                        id="demo-simple-select"
                                                                                        label="Name of degree"
                                                                                        color="success"
                                                                                    >
                                                                                        <MenuItem value={10}>2 Years</MenuItem>
                                                                                        <MenuItem value={20}>3 Years</MenuItem>
                                                                                        <MenuItem value={30}>5 Years</MenuItem>
                                                                                    </Select>
                                                                                </FormControl>
                                                                            </Grid>
                                                                            <Grid item lg={4}>
                                                                                <TextField id="outlined-basic" label="Total fees" variant="outlined" color="success" fullWidth placeholder='Enter Total Fees' />
                                                                            </Grid>
                                                                        </Grid>
                                                                    </div>
                                                                    <div style={{ padding: "15px" }}>
                                                                        <div style={{ padding: "15px" }}>
                                                                            <InputLabel className="poppins_regular">Sample ID</InputLabel>
                                                                        </div>
                                                                        <div>
                                                                            <Grid container columnSpacing={2}>
                                                                                <Grid item lg={4}>
                                                                                    <div className='image_border'>

                                                                                        <img src={user} alt="User Image" style={{ height: '60%', width: '100%' }} />
                                                                                    </div>
                                                                                    <Grid container columnSpacing={2} sx={{ my: 2 }}>
                                                                                        <Grid item lg={6}>
                                                                                            <TextField id="outlined-basic" label="Year" variant="outlined" color="success" fullWidth placeholder='Enter Year' />
                                                                                        </Grid>
                                                                                        <Grid item lg={6}>
                                                                                            <TextField id="outlined-basic" label="Course" variant="outlined" color="success" fullWidth placeholder='Enter Course' />
                                                                                        </Grid>
                                                                                    </Grid>
                                                                                </Grid>
                                                                                <Grid item lg={4}>
                                                                                    <div className='image_border'>
                                                                                        <img src={user} alt="User Image" style={{ height: '60%', width: '100%' }} />
                                                                                    </div>
                                                                                    <Grid container columnSpacing={2} sx={{ my: 2 }}>
                                                                                        <Grid item lg={6}>
                                                                                            <TextField id="outlined-basic" label="Year" variant="outlined" color="success" fullWidth placeholder='Enter Year' />
                                                                                        </Grid>
                                                                                        <Grid item lg={6}>
                                                                                            <TextField id="outlined-basic" label="Course" variant="outlined" color="success" fullWidth placeholder='Enter Course' />
                                                                                        </Grid>
                                                                                    </Grid>
                                                                                </Grid>
                                                                                <Grid item lg={4}>
                                                                                    <div className='image_border' style={{ height: '95%', width: '100%' }}>
                                                                                        <label htmlFor="icon-button-file" >
                                                                                            <Input accept="image/*" id="icon-button-file" type="file" />
                                                                                            <IconButton aria-label="upload picture" component="span" style={{ size: "large", color: "#009D93", fontWeight: "bold", top: "150px", left: "125px" }}>
                                                                                                <AddIcon size="large" />
                                                                                            </IconButton>
                                                                                            <InputLabel className="poppins_regular image_label">Add New</InputLabel>
                                                                                        </label>
                                                                                    </div>
                                                                                </Grid>
                                                                            </Grid>
                                                                        </div>
                                                                    </div>
                                                                    {/* <div style={{ padding: "15px" }}>
                                                                        <div style={{ padding: "15px" }}>
                                                                            <InputLabel className="poppins_regular">Sample ID</InputLabel>
                                                                        </div>
                                                                        <div>
                                                                            <Grid container columnSpacing={2}>
                                                                                <Grid item lg={4}>
                                                                                    <div className='image_border'>

                                                                                        <img src={user} alt="User Image" style={{ height: '60%', width: '100%' }} />
                                                                                    </div>
                                                                                    <Grid container columnSpacing={2} sx={{ my: 2 }}>
                                                                                        <Grid item lg={6}>
                                                                                            <TextField id="outlined-basic" label="Year" variant="outlined" color="success" fullWidth placeholder='Enter Year' />
                                                                                        </Grid>
                                                                                        <Grid item lg={6}>
                                                                                            <TextField id="outlined-basic" label="Course" variant="outlined" color="success" fullWidth placeholder='Enter Course' />
                                                                                        </Grid>
                                                                                    </Grid>
                                                                                </Grid>
                                                                                <Grid item lg={4}>
                                                                                    <div className='image_border'>
                                                                                        <img src={user} alt="User Image" style={{ height: '60%', width: '100%' }} />
                                                                                    </div>
                                                                                    <Grid container columnSpacing={2} sx={{ my: 2 }}>
                                                                                        <Grid item lg={6}>
                                                                                            <TextField id="outlined-basic" label="Year" variant="outlined" color="success" fullWidth placeholder='Enter Year' />
                                                                                        </Grid>
                                                                                        <Grid item lg={6}>
                                                                                            <TextField id="outlined-basic" label="Course" variant="outlined" color="success" fullWidth placeholder='Enter Course' />
                                                                                        </Grid>
                                                                                    </Grid>
                                                                                </Grid>
                                                                                <Grid item lg={4}>
                                                                                    <div className='image_border' style={{ height: '95%', width: '100%' }}>
                                                                                        <label htmlFor="icon-button-file" >
                                                                                            <Input accept="image/*" id="icon-button-file" type="file" />
                                                                                            <IconButton aria-label="upload picture" component="span" style={{ size: "large", color: "#009D93", fontWeight: "bold", top: "150px", left: "125px" }}>
                                                                                                <AddIcon size="large" />
                                                                                            </IconButton>
                                                                                            <InputLabel className="poppins_regular image_label">Add New</InputLabel>
                                                                                        </label>
                                                                                    </div>
                                                                                </Grid>
                                                                            </Grid>
                                                                        </div>
                                                                    </div> */}
                                                                    <div style={{ padding: "0 15px" }}>
                                                                        <Grid container columnSpacing={2} >
                                                                            <Grid item lg={4}>
                                                                                <Button
                                                                                    fullWidth
                                                                                    variant="contained"
                                                                                    className="urban_money_btn m-0 poppins_regular btn-border-radius"
                                                                                >
                                                                                    Save
                                                                                </Button>
                                                                            </Grid>
                                                                            <Grid item lg={4}>
                                                                            </Grid>
                                                                            <Grid item lg={4}>
                                                                            </Grid>
                                                                        </Grid>

                                                                    </div>
                                                                </div>
                                                            </Box>
                                                        </Modal>

                                                        : props?.type === 'delete' ?
                                                            <Modal
                                                                open={props?.open}
                                                                onClose={handleClose}
                                                                aria-labelledby="modal-modal-title"
                                                                aria-describedby="modal-modal-description"
                                                            >
                                                                <Box className="urban_money_modal w-300">
                                                                    <div className='urban_money_modal_title'>
                                                                        <h3 style={{ fontFamily: 'PoppinsMedium' }}>Delete confirmation</h3>
                                                                        <CloseOutlinedIcon onClick={handleClose} style={{ color: '#fff', cursor: 'pointer' }} />
                                                                    </div>
                                                                    <div className='urban_money_modal_content'>
                                                                        <div className='p-20'>
                                                                            <p className='text-center' style={{ fontFamily: 'PoppinsMedium' }}>Are you sure you want to delete this?</p>
                                                                            <Grid container columnSpacing={2} className="mt-15">
                                                                                <Grid item lg={6}>
                                                                                    <Button style={{ fontFamily: 'PoppinsMedium' }} fullWidth variant='outlined' className='urban_money_outlined_btn'>NO</Button>
                                                                                </Grid>

                                                                                <Grid item lg={6}>
                                                                                    <Button style={{ fontFamily: 'PoppinsMedium' }} fullWidth variant='contained' className='urban_money_btn m-0'>YES</Button>
                                                                                </Grid>
                                                                            </Grid>
                                                                        </div>
                                                                    </div>
                                                                </Box>
                                                            </Modal>
                                                            : ''

            }
        </div>
    )
}

export default Modals