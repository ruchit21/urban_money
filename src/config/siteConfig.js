const prod = true;

const siteConfig = {
  siteName: "LMS",
  siteIcon: "",
  footerText: "© 2021 LMS Inc. All rights reserved.",
  GOOGLE_CLIENT_ID:
    "527717493056-3h7oit71tqd13ftnbupms2q62pll2eom.apps.googleusercontent.com",
  // ------------------------------Client Domain----------------------------------------
  // -----------------------------------------------------------------------------------
  domainUrl: prod ? "" : "",
  // apiUrl: prod ? "http://192.168.2.211/lms-api/" : "",
   apiUrl: prod ? "https://lms-api.teamgroovy.com/" : "",
  revalidate: true,
  templateKey: "",
  mapKey: "",
  endpoint: {
    signUp: "v1/user/signup",
    login: "v1/user/login",
    profileUpdate: "v1/user/edit-profile",
    studentList: "v1/user/student-list",
    removeStudent: "v1/user/remove-student",
    courseList: "v1/course/list",
    courseDelete: "v1/course/course-delete",
    courseView: "v1/course/course-view",
    addCourse: "v1/course/add-course",
    updateCourse: "v1/course/update-course",
    inviteStudent: "v1/general/invite",
    invitedStudentList: "v1/general/invitation-list",
    removeInvitation: "v1/general/remove-invitation",
    addSection: "v1/section/add-section",
    deleteSection: "v1/section/delete-section",
    allCourse: "v1/enrolled/all-course",
    myCourse: "v1/enrolled/my-course",
    enrollCourse: "v1/enrolled/add",
    viewLesson: "v1/lesson/view-lesson",
    contactUs: "v1/general/contact-us",
    addLesson: "v1/lesson/add-lesson",
    sectionList: "v1/section/list",
    studentLessonProfile: "v1/user/student-profile",
    deleteLesson: "v1/lesson/delete-lesson",
    appState: "v1/user/update-auth",
    changeCourseStatus: "v1/course/change-status",
    updateSection: "v1/section/update-section",
    updateLesson: "v1/lesson/update-lesson",
    companyData: "v1/general/company-data",
    completeLesson :"v1/enrolled/complete-lesson",
    editCompanyProfile :"v1/user/edit-company",
    viewProfile: 'v1/user/view-profile',
    getFileInfo: 'v1/lesson/get-file-info',
    all: 'v1/enrolled/all-course-list',
    retakeCourse :"v1/enrolled/retake-course",
    changeRoleAPI: "v1/user/change-role"
  },
};

export default siteConfig;
