import React, { useState } from 'react'
import {
    Box,
    Button,
    Grid,
    InputLabel,
    TextField,
    FormControl,
    MenuItem,
    Select,
} from '@mui/material';
import AddIcon from '@material-ui/icons/Add';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from "@mui/material";
import PropTypes from "prop-types";
import BorderColorOutlinedIcon from '@material-ui/icons/BorderColorOutlined';
import DeleteIcon from '@material-ui/icons/Delete';
import PaginationRounded from '../Component/Pagination';
import Modal from '../Component/Modals';

function EnhancedTableHead(props) {

    const headCells = [
        {
            id: "type_of_degree",
            numeric: false,
            disablePadding: true,
            label: "TYPE OF DEGREE",
        },
        {
            id: "name_of_degree",
            numeric: true,
            disablePadding: false,
            label: "NAME OF DEGREE",
        },
        {
            id: "specialization",
            numeric: true,
            disablePadding: false,
            label: "SPECIALIZATION",
        },
        {
            id: "type_of_attendence",
            numeric: true,
            disablePadding: false,
            label: "TYPE OF ATTENDANCE",
        },
        {
            id: "course_tenture",
            numeric: true,
            disablePadding: false,
            label: "COURSE TENTURE",
        },
        {
            id: "total_fees",
            numeric: true,
            disablePadding: false,
            label: "TOTAL FEES",
        },
        {
            id: "sample_id",
            numeric: true,
            disablePadding: false,
            label: "SAMPLE ID",
        },
        {
            id: "sample_report_card",
            numeric: true,
            disablePadding: false,
            label: "SAMPLE REPORT CARD",
        },
        {
            id: "actions",
            numeric: true,
            disablePadding: false,
            label: "ACTIONS",
        },
    ];

    return (
        <TableHead>
            <TableRow >
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align={headCell.numeric ? "left" : "left"}
                        style={{ color: '#009D93', fontFamily: 'PoppinsSemiBold' }}
                    >
                        {headCell.label}
                    </TableCell>
                ))}

            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    rowCount: PropTypes.number.isRequired,
};


const AddCollege = (props) => {
    const [open, setOpen] = React.useState(false);
    const [type, setType] = useState('');

    const handleOpen = (type) => {
        setOpen(true);
        setType(type);
    };

    function createData(type_of_degree, name_of_degree, specialization, type_of_attendence, course_tenture, total_fees, sample_id, sample_report_card, actions) {
        return {
            type_of_degree,
            name_of_degree,
            specialization,
            type_of_attendence,
            course_tenture,
            total_fees,
            sample_id,
            sample_report_card,
            actions,
        };
    }

    const College_data = [
        createData(
            "Master",
            "Msc",
            "Forestry",
            "Full time",
            "2 years",
            "$ 2,00,000",
            <Button variant="outlined" color="success">View</Button>,
            <Button variant="outlined" color="success">View</Button>,
        ),
        createData(
            "Master",
            "Msc",
            "Data Analytics",
            "Part time",
            "3 years",
            "$ 2,00,000",
            <Button variant="outlined" color="success">View</Button>,
            <Button variant="outlined" color="success">View</Button>,
        ),
        createData(
            "Master",
            "MBA",
            "HR",
            "Full time",
            "2 years",
            "$ 2,00,000",
            <Button variant="outlined" color="success">View</Button>,
            <Button variant="outlined" color="success">View</Button>,
        ),
        createData(
            "Master",
            "MBA",
            "Marketing",
            "Part time",
            "3 years",
            "$ 2,00,000",
            <Button variant="outlined" color="success">View</Button>,
            <Button variant="outlined" color="success">View</Button>,
        ),
        createData(
            "Bachelors",
            "Bsc",
            "Marketing",
            "Part time",
            "3 years",
            "$ 2,00,000",
            <Button variant="outlined" color="success">View</Button>,
            <Button variant="outlined" color="success">View</Button>,
        ),
        createData(
            "Bachelors",
            "Bsc",
            "Marketing",
            "Full time",
            "2 years",
            "$ 2,00,000",
            <Button variant="outlined" color="success">View</Button>,
            <Button variant="outlined" color="success">View</Button>,
        ),
        createData(
            "Bachelors",
            "BBA",
            "Marketing",
            "Part time",
            "3 years",
            "$ 2,00,000",
            <Button variant="outlined" color="success">View</Button>,
            <Button variant="outlined" color="success">View</Button>,
        ),
        createData(
            "Bachelors",
            "BBA",
            "Marketing",
            "Full time",
            "2 years",
            "$ 2,00,000",
            <Button variant="outlined" color="success">View</Button>,
            <Button variant="outlined" color="success">View</Button>,
        ),

    ];


    return (
        <div style={{ width: "100%", margin: "10px 0 10px 10px" }}>
            <div className="urban_money_page_content">
                <div className="urban_money_page_title">
                    <h2 style={{ margin: "0" }}>
                        {/* <b> {props?.routeName} </b> */}
                        <b> Add new college details</b>
                    </h2>
                </div>
                <div style={{ padding: "20px" }}>
                    <Box sx={{ width: "100%" }} style={{ boxShadow: "rgba(0, 0, 0, 0.15) 0px 0.5rem 1rem", borderRadius: "5px" }}>
                        <div style={{ padding: "15px" }}>
                            <Grid container columnSpacing={2}>
                                <Grid item lg={9}>
                                    <TextField id="outlined-basic" label="Name of College" variant="outlined" color="success" fullWidth placeholder='Enter name of College' />
                                </Grid>
                                <Grid item lg={3}>
                                    <TextField id="outlined-basic" label="No. of Students" variant="outlined" color="success" fullWidth placeholder='Enter no. of Students' />
                                </Grid>
                            </Grid>
                        </div>
                        <div style={{ padding: "15px" }}>
                            <Grid container columnSpacing={2}>
                                <Grid item lg={3}>
                                    <TextField id="outlined-basic" label="Minimun fees" variant="outlined" color="success" fullWidth placeholder='Enter Minimun fees' />
                                </Grid>
                                <Grid item lg={3}>
                                    <TextField id="outlined-basic" label="Maximun fees" variant="outlined" color="success" fullWidth placeholder='Enter Maximun fees' />
                                </Grid>
                                <Grid item lg={3}>
                                    <TextField id="outlined-basic" label="Mean fees" variant="outlined" color="success" fullWidth placeholder='Enter Mean fees' />
                                </Grid>
                                <Grid item lg={3}>
                                    <TextField id="outlined-basic" label="Median fees" variant="outlined" color="success" fullWidth placeholder='Enter Median fees' />
                                </Grid>
                            </Grid>
                        </div>
                        <div style={{ padding: "15px" }}>
                            <InputLabel className='poppins_semi mb-15' >Address</InputLabel>
                            <Grid container columnSpacing={2}>
                                <Grid item lg={6}>
                                    <TextField id="outlined-basic" label="Address line 1" variant="outlined" color="success" fullWidth placeholder='Enter Address Line 1' />
                                </Grid>
                                <Grid item lg={6}>
                                    <TextField id="outlined-basic" label="Address line 2" variant="outlined" color="success" fullWidth placeholder='Enter Address Line 2' />
                                </Grid>
                            </Grid>
                        </div>
                        <div style={{ padding: "15px" }}>
                            <Grid container columnSpacing={2}>
                                <Grid item lg={3}>
                                    <FormControl fullWidth>
                                        <InputLabel className="poppins_regular" color="success">
                                            State
                                        </InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            label="State"
                                            color="success"
                                        >
                                            <MenuItem value={10}>Delhi</MenuItem>
                                            <MenuItem value={20}>Gujarat</MenuItem>
                                            <MenuItem value={30}>Maharashtra</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item lg={3}>
                                    <FormControl fullWidth>
                                        <InputLabel className="poppins_regular" color="success">
                                            City
                                        </InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            label="City"
                                            color="success"
                                        >
                                            <MenuItem value={10}>Nadiad</MenuItem>
                                            <MenuItem value={20}>Rajkot</MenuItem>
                                            <MenuItem value={30}>Patan</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item lg={3}>
                                    <TextField id="outlined-basic" label="Pincode" variant="outlined" color="success" fullWidth placeholder='Enter Pincode' />
                                </Grid>
                                <Grid item lg={3}>
                                    {/* <TextField id="outlined-basic" label="Median fees" variant="outlined" fullWidth /> */}
                                </Grid>
                            </Grid>
                        </div>
                        <div style={{ padding: "15px" }}>
                            <Grid container columnSpacing={2}>
                                <Grid item lg={3}>
                                    <Button
                                        fullWidth
                                        variant="contained"
                                        className="urban_money_filter_btn btn-border-radius poppins_regular"
                                    >
                                        Save
                                    </Button>
                                </Grid>
                            </Grid>
                        </div>
                    </Box>
                </div>
                <div style={{ padding: "0 20px 20px 20px" }}>
                    <Grid container columnSpacing={2}>
                        <Grid item lg={8}>
                            <InputLabel className='poppins_semi mb-15'>Course Details</InputLabel>
                        </Grid>
                        <Grid item lg={4}>
                            <Grid container columnSpacing={2}>
                                <Grid item lg={5}></Grid>
                                <Grid item lg={4} align="right">
                                    <Button variant="contained" className='urban_money_btn m-0'>Upload SCV</Button>
                                </Grid>
                                <Grid item lg={3} align="right">
                                    <Button variant="contained" className='urban_money_btn m-0' onClick={() => handleOpen('add-course-details')}> <AddIcon style={{ marginRight: "5px" }} />Add</Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Box sx={{ width: "100%" }}>
                        <div style={{ margin: '20px 0', boxShadow: '0 .5rem 1rem rgba(0,0,0,.15)', borderRadius: '5px' }}>
                            <TableContainer>
                                <Table
                                    sx={{ minWidth: 750 }}
                                    aria-labelledby="tableTitle"
                                >
                                    <EnhancedTableHead
                                        rowCount={College_data.length}
                                    />
                                    <TableBody>
                                        {
                                            College_data.map((row, index) => {
                                                return (
                                                    <TableRow
                                                        hover
                                                        tabIndex={-1}
                                                        key={row.name}
                                                    >

                                                        <TableCell> {row.type_of_degree} </TableCell>
                                                        <TableCell align="left">{row.name_of_degree}</TableCell>
                                                        <TableCell align="left">{row.specialization}</TableCell>
                                                        <TableCell align="left">{row.type_of_attendence}</TableCell>
                                                        <TableCell align="left">{row.course_tenture}</TableCell>
                                                        <TableCell align="left">{row.total_fees}</TableCell>
                                                        <TableCell align="left">{row.sample_id}</TableCell>
                                                        <TableCell align="left">{row.sample_report_card}</TableCell>

                                                        <TableCell >
                                                            <BorderColorOutlinedIcon style={{ color: '#009D93', margin: "5px" }} />
                                                            <DeleteIcon style={{ color: '#DE1717', margin: "5px" }} onClick={() => handleOpen('delete')} />
                                                        </TableCell>
                                                    </TableRow>
                                                );
                                            })}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                            <div>
                                <PaginationRounded />
                            </div>
                        </div>
                    </Box>
                </div>
            </div>
            <Modal type={type} open={open} setOpen={setOpen} />
        </div>
    )
}

export default AddCollege